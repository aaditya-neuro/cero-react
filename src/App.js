import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import '../src/assets/css/style.css'

import ProtectedRoute from './ProtectedRoute';

import Login from './Components/login/Login';

import Lead from './Components/leads/Lead'
import AddLead from './Components/leads/AddLead'
import Editlead from './Components/leads/Editlead';

import Users from './Components/users/Users';
import Addusers from './Components/users/Adduser'
import Edituser from './Components/users/Edituser';

import Roles from './Components/roles/Roles'
import Addroles from './Components/roles/Addroles'
import Editroles from './Components/roles/Editroles'

import Profile from './Components/profile/Profile';
import Editprofile from './Components/profile/Editprofile';

import AddPlant from './Components/Plant/AddPlant'
import Plant from './Components/Plant/Plant'
import Editplant from './Components/Plant/Editplant'

import Sidebar from './Components/layout/Sidebar';
import Header from './Components/layout/Header';


import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import AddVehicleType from './Components/master/vehicle type/AddVehicleType';
import AddVehicleCategory from './Components/master/vehicle category/AddVehicleCategory';
import vehicleCategory from './Components/master/vehicle category/VehicleCategory';
import VehicleType from './Components/master/vehicle type/VehicleType';
import EditVehicleCategory from './Components/master/vehicle category/EditVehicleCategory';
import EditVehicleType from './Components/master/vehicle type/EditVehicleType';
import VehicleScrapPrice from './Components/master/vehicle scrap price/VehicleScrapPrice';
import AddVehicleScrapPrice from './Components/master/vehicle scrap price/AddVehicleScrapPrice';
import EditVehicleScrapPrice from './Components/master/vehicle scrap price/EditVehicleScrapPrice';
import AddCountry from './Components/master/Location/Country/AddCountry';
import EditCountry from './Components/master/Location/Country/EditCountry'
import Country from './Components/master/Location/Country/Country'
import AddState from './Components/master/Location/State/AddState'
import EditState from './Components/master/Location/State/EditState'
import State from './Components/master/Location/State/State'
import AddCity from './Components/master/Location/City/AddCity'
import EditCity from './Components/master/Location/City/EditCity'
import City from './Components/master/Location/City/City'
import AddPincode from './Components/master/Location/Pincode/AddPincode'
import EditPincode from './Components/master/Location/Pincode/EditPincode'
import Pincode from './Components/master/Location/Pincode/Pincode'
import LeadListingCustomerCare from './Components/leads/customerCare/LeadListingCustomerCare';
import EditLeadCustomerCare from './Components/leads/customerCare/EditLeadCustomerCare';
import Rules from './Components/users/Rules';
import AddMake from './Components/master/Make/AddMake';
import EditMake from './Components/master/Make/EditMake';
import Make from './Components/master/Make/Make';
import AddModel from './Components/master/Model/AddModel';
import EditModel from './Components/master/Model/EditModel';
import Model from './Components/master/Model/Model';

function App() {
  // const context = useContext(AuthContext)
  // const { isAuthenticated, setIsauthenticated } = context

  return (
    <>
      <Router>

        <Switch>

          <Route exact path="/">

          <Login />

          </Route>
          
          <Route>

            <Sidebar />

            <Header />

           
            <ProtectedRoute exact path="/add-lead" Component={AddLead} />

            <ProtectedRoute exact path="/Lead" Component={Lead} />

            <ProtectedRoute exact path="/edit-lead" Component={Editlead} />

            <ProtectedRoute exact path="/User" Component={Users} />

            <ProtectedRoute exact path="/add-user" Component={Addusers} />

            <ProtectedRoute exact path="/roles" Component={Roles} />

            <ProtectedRoute exact path="/add-role" Component={Addroles} />

            <ProtectedRoute exact path="/edit-roles" Component={Editroles} />

            <ProtectedRoute exact path="/edit-user" Component={Edituser} />

            <ProtectedRoute exact path="/profile" Component={Profile} />

            <ProtectedRoute exact path="/profile-edit" Component={Editprofile} />

            <ProtectedRoute exact path="/add-plant" Component={AddPlant} />

            <ProtectedRoute exact path="/plant" Component={Plant} />

            <ProtectedRoute exact path="/edit-plant" Component={Editplant} />

            <ProtectedRoute exact path="/add-vehicleType" Component={AddVehicleType} />

            <ProtectedRoute exact path="/vehicleType" Component={VehicleType} />

            <ProtectedRoute exact path="/edit-vehicleType" Component={EditVehicleType} />

            <ProtectedRoute exact path="/add-vehicleCategory" Component={AddVehicleCategory} />

            <ProtectedRoute exact path="/edit-vehicleCategory" Component={EditVehicleCategory} />

            <ProtectedRoute exact path="/vehicleCategory" Component={vehicleCategory} />

            <ProtectedRoute exact path="/vehicleScrapPrice" Component={VehicleScrapPrice} />

            <ProtectedRoute exact path="/add-vehicleScrapPrice" Component={AddVehicleScrapPrice} />

            <ProtectedRoute exact path="/edit-vehicleScrapPrice" Component={EditVehicleScrapPrice} />

            <ProtectedRoute exact path="/add-country" Component={AddCountry} />

            <ProtectedRoute exact path="/edit-country" Component={EditCountry} />

            <ProtectedRoute exact path="/Country" Component={Country} />

            <ProtectedRoute exact path="/add-state" Component={AddState} />

            <ProtectedRoute exact path="/edit-state" Component={EditState} />

            <ProtectedRoute exact path="/State" Component={State} />

            <ProtectedRoute exact path="/add-city" Component={AddCity} />

            <ProtectedRoute exact path="/edit-city" Component={EditCity} />

            <ProtectedRoute exact path="/City" Component={City} />

            <ProtectedRoute exact path="/add-Pincode" Component={AddPincode} />

            <ProtectedRoute exact path="/edit-pincode" Component={EditPincode} />

            <ProtectedRoute exact path="/Pincode" Component={Pincode} />

            <ProtectedRoute exact path="/add-make" Component={AddMake} />

            <ProtectedRoute exact path="/edit-make" Component={EditMake} />

            <ProtectedRoute exact path="/Make" Component={Make} />

            <ProtectedRoute exact path="/add-model" Component={AddModel} />

            <ProtectedRoute exact path="/edit-model" Component={EditModel} />

            <ProtectedRoute exact path="/Model" Component={Model} />

            <ProtectedRoute exact path="/Lead-customerCare" Component={LeadListingCustomerCare} />
            <ProtectedRoute exact path="/edit-Lead-customerCare" Component={EditLeadCustomerCare} />
            <ProtectedRoute exact path="/rules" Component={Rules} />


          </Route>

        </Switch>

      </Router>

    </>
  );
}

export default App;
