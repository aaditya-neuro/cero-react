const validate = (values) => {

    const formErrors = {}
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;

    if (!values.email) {
        formErrors.email = "email is required !"
    } else if (!regex.test(values.email)) {
        formErrors.email = "Please enter a valid email .";
    }

    if (!values.password) {
        formErrors.password = "password is required !"
    }

    if (!values.contactNumber) {
        formErrors.contactNumber = "Contact number is required !"
    } else if (values.contactNumber.length !== 10) {
        formErrors.contactNumber = "Please fill the correct contact number !"
    }
    // for adding lead

    if (!values.firstName) {
        formErrors.firstName = "First name is required !"
    }
    if (!values.lastName) {
        formErrors.lastName = "Last name is required !"
    }

    // for adding user / editing user

    if (!values.name) {
        formErrors.name = " name is required !"
    }
    if (!values.userName) {
        formErrors.userName = " User name is required !"
    }
    if (!values.role) {
        formErrors.role = "Please select role !"
    }

    if (!values.branch) {
        formErrors.branch = "Please select branch !"
    }
    if (!values.address) {
        formErrors.address = "Address is required !"
    }
    if(!values.status){
        formErrors.status = "status is required !"
    }
    // for adding  roles

    if (!values.roleName) {
        formErrors.roleName = "Please add role !"

    }
    if (values.userSelected === undefined || values.userSelected === null) {
        formErrors.userSelected = "Please select user !"
    }

    //edit profile

    if (!values.oldPassword) {
        formErrors.oldPassword = "Please enter old password !"

    }
    if (!values.newPassword) {
        formErrors.newPassword = "Please enter new password !"

    }
    if (!values.confirmPassword) {
        formErrors.confirmPassword = "confirm  password can't be empty !"

    }else if(values.confirmPassword===!values.newPassword){
        formErrors.confirmPassword = "New password and confirm password should be same !"
    }

    return formErrors

}
export default validate;