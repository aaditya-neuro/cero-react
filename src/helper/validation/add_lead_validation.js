const validation = (values) => {

    const formErrors = {}

    if (!values.title) {
        formErrors.title = "title is required !"
    }
    if (!values.name) {
        formErrors.name = "name is required !"
    }
    if (!values.email) {
        formErrors.email = "email is required !"
    }
    if (!values.contactNumber) {
        formErrors.contactNumber = "contactNumber is required !"
    }
    if (!values.manufacturer) {
        formErrors.manufacturer = "manufacturer is required !"
    }
    if (!values.location) {
        formErrors.location = "location is required !"
    }
    if (!values.model) {
        formErrors.model = "model is required !"
    }
    if (!values.registrationyear) {
        formErrors.registrationyear = "registrationyear is required !"
    }
    if (!values.sellerType) {
        formErrors.sellerType = "sellerType is required !"
    }
    if (!values.status) {
        formErrors.status = "Status is required !"
    }
    if (!values.referrerName) {
        formErrors.referrerName = "referrerName is required !"
    }
    if (!values.ReferrerContactNumber) {
        formErrors.ReferrerContactNumber = "ReferrerContactNumber is required !"
    }
  

    return formErrors
}
export default validation;