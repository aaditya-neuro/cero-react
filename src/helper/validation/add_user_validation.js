const validate = (values) => {

    const formErrors = {}
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;

    if (!values.email) {
        formErrors.email = "email is required !"
    } else if (!regex.test(values.email)) {
        formErrors.email = "Please enter a valid email .";
    }

    if (!values.password) {
        formErrors.password = "password is required !"
    }
    if (!values.confirmPassword) {
        formErrors.confirmPassword = " confirm password is required !"
    }else if(values.confirmPassword!==values.password){
        formErrors.confirmPassword = " confirm password is same as password !"
    }
    if (!values.contactNumber) {
        formErrors.contactNumber = "Contact number is required !"
    } else if (values.contactNumber.length !== 10) {
        formErrors.contactNumber = "Please fill the correct contact number !"
    }
    if (!values.userName) {
        formErrors.userName = " User name is required !"
    }
    if (!values.roleId) {
        formErrors.roleId = "Please select role !"
    }

    if (!values.roleTypeId) {
        formErrors.roleTypeId = "Please select User type !"
    }
    if (!values.address) {
        formErrors.address = "Address is required !"
    }
    if(!values.status){
        formErrors.status = "status is required !"
    }
    if(!values.cityId){
        formErrors.cityId = "city is required !"
    }
    // for adding  roles

    return formErrors

}
export default validate;