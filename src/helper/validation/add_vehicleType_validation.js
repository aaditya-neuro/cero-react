const validation = (values) => {

    const formErrors = {}

    if (!values.vehicleName) {
        formErrors.vehicleName = "vehicleName is required !"
    }
    if (!values.vehicleCategory) {
        formErrors.vehicleCategory = "vehicleCategory is required !"
    }
    if (!values.status) {
        formErrors.status = "status is required !"
    }
    return formErrors
}
export default validation;