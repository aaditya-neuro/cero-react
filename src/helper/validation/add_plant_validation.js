const validation = (values) => {

    const formErrors = {}

    if (!values.plantName) {
        formErrors.plantName = "plantName is required !"
    }
    if (!values.state) {
        formErrors.state = "State is required !"
    }
    if (!values.city) {
        formErrors.city = "City is required !"
    }
    if (!values.address) {
        formErrors.address = "address is required !"
    }
  

    return formErrors
}
export default validation;