const avoidAlphabets = (event) => {
    var k = event ? event.which : window.event.keyCode;
    if (k >= 48 && k <= 57) {
      return true

    } else {
      event.preventDefault()
    }
  }
  const avoidSpace = (event) => {
    var k = event ? event.which : window.event.keyCode;
    if (k === 32) {
      event.preventDefault();

    }
  }
  export {avoidAlphabets,avoidSpace}