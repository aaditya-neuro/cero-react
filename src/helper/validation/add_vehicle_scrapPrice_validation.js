const validation = (values) => {

    const formErrors = {}

    if (!values.vehicleCategoryId) {
        formErrors.vehicleCategoryId = "vehicle Category is required !"
    }
    if (!values.vehicleTypeId) {
        formErrors.vehicleTypeId = "vehicle Type is required !"
    }
    if (!values.vehicleScrapBasePrice) {
        formErrors.vehicleScrapBasePrice = "vehicle Scrap Base Price is required !"
    }
    if (!values.status) {
        formErrors.status = "status is required !"
    }
    return formErrors
}
export default validation;