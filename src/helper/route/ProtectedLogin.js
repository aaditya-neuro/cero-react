import React from 'react';
import { Route,Redirect } from 'react-router-dom';
const ProtectedLogin = ({Component,...rest}) => {
    var auth = false
    if(localStorage.getItem("auth-token")){
        auth = true
    }
    return(
        <Route {...rest} render={(props)=>{
          if(!auth){
                return <Component{...props}/>
            } else{
                return <Redirect to={{pathname:"/Lead"}}/>
            }
        }}/>
    )
};

export default ProtectedLogin;
