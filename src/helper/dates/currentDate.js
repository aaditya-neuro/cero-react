const currentDate = () => {
  var currentYear = new Date().getFullYear()
  var currentMonth
  var currentDate = new Date().getDate()
  switch (new Date().getMonth()) {
    case 0:
      currentMonth = "Jan"
      break;
    case 1:
      currentMonth = "Feb"
      break;
    case 2:
      currentMonth = "Mar"
      break;
    case 3:
      currentMonth = "Apr"
      break;
    case 4:
      currentMonth = "May"
      break;
    case 5:
      currentMonth = "Jun"
      break;
    case 6:
      currentMonth = "Jul"
      break;
    case 7:
      currentMonth = "Aug"
      break;
    case 8:
      currentMonth = "Sep"
      break;
    case 9:
      currentMonth = "Oct"
      break;
    case 10:
      currentMonth = "Nov"
      break;
    case 11:
      currentMonth = "Dec"
      break;

  }
  if (currentDate <= 9) {
    currentDate = "0" + currentDate
  }
  return { currentYear, currentDate, currentMonth }


}

export default currentDate;
