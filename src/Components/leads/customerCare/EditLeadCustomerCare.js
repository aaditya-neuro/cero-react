import React, { useState, useEffect } from 'react'
import { avoidAlphabets, avoidSpace } from '../../../helper/validation/keypressValidation'
import validate from '../../../helper/validation/add_lead_validation'
import Alert from '../../layout/Alert'
import axios from 'axios'
import $ from 'jquery'
import { baseUrl3 } from '../../../helper/constants/constant'
import { useHistory } from 'react-router-dom'

const EditLeadCustomerCare = (props) => {
  const history = useHistory()
  const back = () => {
    history.goBack()
  }
  const previousData = props.history.location.state
  console.log(previousData)

  

  useEffect(() => {
    if (!previousData){
      //console.log("if called")
      $("#defaultmodal").modal("show")
    }
    else {
      setUserData({
        title: previousData.salutation,
        name: previousData.name,
        email: previousData.email,
        contactNumber: previousData.contactNumber,
        location: previousData.location,
        manufacturer: previousData.manufacturer,
        model: previousData.model,
        registrationyear: previousData.registrationYear,
        sellerType: previousData.sellerType,
        status: previousData.leadStatus
      })
      if(previousData.sellerType=="Individual"){
        $("#customRadioInline1").attr("checked",true);
      }else{
        $("#customRadioInline2").attr("checked",true);
      }
      if(previousData.isReferral===true){
        $("#customSwitch1").attr("checked",true)
      

    }
      console.log("else called")
      }

  
    },  [])

  const [userData, setUserData] = useState({
    title: previousData.salutation,
    name: previousData.name,
    email: previousData.email,
    contactNumber: previousData.contactNumber,
    location: previousData.location,
    manufacturer: previousData.make,
    model: previousData.model,
    registrationyear: previousData.registrationYear,
    sellerType: previousData.sellerType,
    status: previousData.leadStatus
  })

  const [error, seterror] = useState({})
  const [isSubmit, setIsSubmit] = useState(false)
  const [alert, setAlert] = useState(null)
  const [isReferral, setisReferral] = useState(previousData.isReferral)
  const [lengthError, setLengthError] = useState(null)


   const [authForRoute, setAuthForRoute] = useState(false)

  

  const showAlert = (message, type) => {
    setAlert({
      message: message,
      type: type
    })
    setTimeout(() => {
      setAlert(null)
    }, 2000);
  }
  useEffect(() => {
    if (isReferral === true) {
      setUserData({ ...userData, referrerName:previousData.referrarName, ReferrerContactNumber:previousData.referrerContactNumber })
    } else {
      setUserData({ ...userData })
    }
  }, [isReferral])
  useEffect(() => {
    if (Object.keys(error).length === lengthError && isSubmit) {
      console.log(userData);
      const body = {
        "id":previousData.id,
        "leadId": 2,
        "salutation": userData.title,
        "name": userData.name,
        "contactNumber": userData.contactNumber,
        "email": userData.email,
        "location": userData.location,
        "make": userData.manufacturer,
        "model": userData.model,
        "registrationYear": userData.registrationyear,
        "sellerType": userData.sellerType,
        "isReferral": isReferral,
        "referrarName": userData.referrerName,
        "referrerContactNumber": userData.ReferrerContactNumber,
        "leadStatus": userData.status
      }
      axios.post(`${baseUrl3}/api/v1/leads/editLeadDetails`, body)
      .then((response) => {
        console.log(response.data);
        if (response.data.status === "success") {
          showAlert("Lead edited successfully.", "success")
          setUserData({
            title: "",
            name: "",
            email: "",
            contactNumber: "",
            location: "",
            manufacturer: "",
            model: "",
            registrationyear: "",
            sellerType: "",
            referrerName: "",
            ReferrerContactNumber: ""
          })
          setTimeout(() => {
            history.push('/Lead')

          }, 2000);

        } else {
          showAlert("Some error occur.", "danger")
        }

      })
      //api call here
    }
  }, [error])

  const onChange = (e) => {
    setUserData({ ...userData, [e.target.name]: e.target.value })
    setIsSubmit(false)
    seterror({})
  }
  const handleSubmit = (e) => {
    setLengthError(2)
    seterror(validate(userData))
    setIsSubmit(true)
    if (isReferral === true) {
      setLengthError(0)
    }
  }
  const reset = () => {
    setUserData({
      title: "",
      name: "",
      email: "",
      contactNumber: "",
      location: "",
      manufacturer: "",
      model: "",
      registrationyear: "",
      sellerType: "",
      referrerName: "",
      ReferrerContactNumber: "",
      status: ''
    })
  }
  const checkeRferral = () => {
    isReferral == false ?
      setisReferral(true) :
      setisReferral(false)
  }
  const onDetailsChange = (e) => {
    setDetails({ ...details, [e.target.name]: e.target.value })
  }


  const [details, setDetails] = useState({
    vendorType: "",
    gstNumber: "",
    vendorCode: "",
    leadType: "",
    leadState: ""
  })
  const submitDetails = () => {
    console.log(details);
  }
  return (
    <>
    

      

     <div className='dashboard'>

      <div className=' dashboard-content'>

        {/* Modal */}

        <div class="modal fade " id="moreDetails" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">

          <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">

            <div class="modal-content">

              <div class="modal-header">

                <h5 class="modal-title" id="exampleModalLabel"> Vendor Details</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                  <span aria-hidden="true">&times;</span>

                </button>

              </div>

              <div class="modal-body">

                <div className="row">

                  <div className="col-md-6 form-group">

                    <label for="exampleFormControlSelect1">vendor Type <span className="text-red">*</span></label>

                    <select className="form-control" id="exampleFormControlSelect1"
                      onChange={onDetailsChange}
                      value={details.vendorType}
                      name="vendorType"
                    >
                      <option value=''>Select vendor Type </option>
                      <option value='Individual'>Individual</option>
                      <option value='Insitutional'>Insitutional</option>

                    </select>

                  </div>

                  <div className="col-md-6 form-group">

                    <label htmlFor="">Gst Number <span className="text-red">*</span></label>

                    <input type="text" className="form-control"
                      placeholder="Enter GST number here"
                      onChange={onDetailsChange}
                      value={details.gstNumber}
                      name="gstNumber"
                    />

                  </div>

                  <div className="col-md-6 form-group">

                    <label htmlFor="">vendor code <span className="text-red">*</span></label>

                    <input type="text" className="form-control"
                      placeholder="Enter vendor codr here"
                      onChange={onDetailsChange}
                      value={details.vendorCode}
                      name="vendorCode"
                    />

                  </div>

                  <div className="col-md-6 form-group">

                    <label for="exampleFormControlSelect1">Lead Type <span className="text-red">*</span></label>

                    <select className="form-control" id="exampleFormControlSelect1"
                      onChange={onDetailsChange}
                      value={details.leadType}
                      name="leadType"
                    >
                      <option value=''>Select Lead Type </option>
                      <option value='B2B'>B2B</option>
                      <option value='Retail'>Retail</option>

                    </select>

                  </div>

                  <div className="col-md-6 form-group">

                    <label for="exampleFormControlSelect1">Lead State <span className="text-red">*</span></label>

                    <select className="form-control" id="exampleFormControlSelect1"
                      onChange={onDetailsChange}
                      value={details.leadState}
                      name="leadState"
                    >
                      <option>select State</option>
                      <option value="Hot">Hot</option>
                      <option value="Cold">Cold</option>
                      <option value="Garbage">Garbage</option>
                      </select>

                  </div>

                </div>


              </div>

              <div class="modal-footer">

                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                <button type="button" class="btn btn-primary" onClick={submitDetails}>Submit details</button>

              </div>

            </div>

          </div>

        </div>

        <header className="title-head mb-4">

          <h1 className="h4">Edit Lead for customercare</h1>

        </header>

        <Alert alert={alert} />

        <div className="bg-white rounded p-4">

          <div className="row">

            <div className="col-md-6 form-group">

              <label for="exampleFormControlSelect1">Title  <span className="text-red">*</span></label>

              <select className="form-control" id="exampleFormControlSelect1"
                onChange={onChange}
                value={userData.title}
                name="title"
              >
                <option value='Mr.'>Mr</option>
                <option value='Mrs.'>Mrs</option>
                <option value='Ms.'>Ms</option>
              </select>
              <p className="text-red">{error.title}</p>
            </div>

            <div className="col-md-6 form-group">

              <label htmlFor=""> Name <span className="text-red">*</span></label>

              <input type="text" className="form-control"
                placeholder="Full Name"
                onChange={onChange}
                value={userData.name}
                name="name"
              />

              <p className="text-red">{error.name}</p>

            </div>

            <div className="col-md-6 form-group">

              <label htmlFor="">Email<span className="text-red">*</span></label>

              <input type="text" className="form-control"
                onKeyPress={avoidSpace}
                placeholder="Email"
                onChange={onChange}
                value={userData.email}
                name="email"
              />

              <p className="text-red">{error.email}</p>

            </div>

            <div className="col-md-6 form-group">

              <label htmlFor="">Contact Number<span className="text-red">*</span></label>

              <input type="text" className="form-control"
                onKeyPress={avoidAlphabets}
                placeholder="Contact Number"
                onChange={onChange}
                value={userData.contactNumber}
                name="contactNumber"
                maxLength={10}
              />

              <p className="text-red">{error.contactNumber}</p>

            </div>

            <div className="col-md-6 form-group">

              <label htmlFor=""> Location <span className="text-red">*</span></label>

              <input type="text" className="form-control"
                placeholder="Location of car"
                onChange={onChange}
                value={userData.location}
                name="location"

              />
              <p className="text-red">{error.location}</p>

            </div>

            <div className="col-md-6 form-group">

              <label htmlFor="">Manufacturer <span className="text-red">*</span></label>

              <input type="text" className="form-control"
                placeholder="Manufacturer company of car"
                onChange={onChange}
                value={userData.manufacturer}
                name="manufacturer"
              />
              <p className="text-red">{error.manufacturer}</p>

            </div>

            <div className="col-md-6 form-group">

              <label htmlFor=""> Model <span className="text-red">*</span></label>

              <input type="text" className="form-control"
                placeholder="Model of car"
                onChange={onChange}
                value={userData.model}
                name="model"
              />
              <p className="text-red">{error.model}</p>

            </div>

            <div className="col-md-6 form-group">

              <label htmlFor=""> Registration year <span className="text-red">*</span></label>

              <input type="text" className="form-control"
                onKeyPress={avoidAlphabets}
                placeholder="Registration Year"
                onChange={onChange}
                value={userData.registrationyear}
                name="registrationyear"
                maxLength={4}
              />
              <p className="text-red">{error.registrationyear}</p>

            </div>

            <div className="col-md-6 form-group">

              <label htmlFor=""> Seller Type <span className="text-red">*</span></label>

              <br />

              <div className="custom-control custom-radio custom-control-inline">

                <input type="radio" id="customRadioInline1" name="sellerType" className="custom-control-input"
                  onChange={onChange}
                  value="Individual"

                />


                <label className="custom-control-label" for="customRadioInline1">Individual (direct)</label>

              </div>

              <div className="custom-control custom-radio custom-control-inline">

                <input type="radio" id="customRadioInline2" name="sellerType" className="custom-control-input"
                  onChange={onChange}
                  value="Institutional"
                />


                <label className="custom-control-label" for="customRadioInline2"> Institutional (indirect)</label>

              </div>
              <p className="text-red">{error.sellerType}</p>
            </div>

            <div className="col-md-6 form-group">

              <br />

              <div className="custom-control custom-switch">

                <input type="checkbox" className="custom-control-input" id="customSwitch1"
                  onClick={checkeRferral}

                />

                <label className="custom-control-label" for="customSwitch1">Is referral ? </label>

              </div>

            </div>
            {

              isReferral == true ?

                <>

                  <div className="col-md-6 form-group">

                    <label htmlFor="">Referrer name <span className="text-red">*</span></label>

                    <input type="text" className="form-control"
                      placeholder="Referrer Name"
                      onChange={onChange}
                      value={userData.referrerName}
                      name="referrerName"
                    />

                    <p className="text-red">{error.referrerName}</p>

                  </div>

                  <div className="col-md-6 form-group">

                    <label htmlFor=""> Referrer Contact Number<span className="text-red">*</span></label>

                    <input type="text" className="form-control"
                      onKeyPress={avoidAlphabets}
                      placeholder="Contact Number"
                      onChange={onChange}
                      value={userData.ReferrerContactNumber}
                      name="ReferrerContactNumber"
                      maxLength={10}
                    />

                    <p className="text-red">{error.ReferrerContactNumber}</p>

                  </div>
                </>
                : null

            }

            <div className="col-md-6 form-group">

              <label for="exampleFormControlSelect1">Status  <span className="text-red">*</span></label>

              <select className="form-control" id="exampleFormControlSelect1"
                onChange={onChange}
                value={userData.status}
                name="status"
              >

                <option value=''>Select Lead Status </option>
                <option value='New'>New</option>
                <option value='qualifiedLead'>Qualified Lead</option>
                <option value='junkLead'>Junk Lead</option>
                <option value='converted'>Converted</option>

              </select>
              <p className="text-red">{error.status}</p>
            </div>


            <div className="col-md-6 form-group my-3">

              <button type="button" class="btn btn-outline-primary  mt-2 " data-toggle="modal" data-target="#moreDetails">

                Fill some More details

              </button>

            </div>

          </div>

          <div className="row row-gap justify-content-center mt-5 mb-4">

            <div className="col-6 col-lg-2 col-md-3">

              <button type="reset" className="btn btn-outline-primary btn-block" onClick={reset}>Reset</button>

            </div>

            <div className="col-6 col-lg-2 col-md-3">

              <button type="submit" className="btn btn-primary btn-block" onClick={handleSubmit}>Submit</button>

            </div>

          </div>

        </div>

      </div>

    </div> :
      
      <div className='dashboard'>

      <div className='dashboard-content'>

        <div class="modal fade " id="defaultmodal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true" >

          <div class="modal-dialog modal-dialog-centered">

            <div class="modal-content">

              <div class="modal-header">

                <h4 class="modal-title" id="staticBackdropLabel">Unauthorised User Please go back to previous page</h4>

              </div>

              <div class="modal-body d-flex justify-content-center">

                <button class="btn btn-primary" type="submit" onClick={back}>Ok</button>

              </div>

            </div>

          </div>

        </div>

      </div>

    </div>

    </>
  )
}

export default EditLeadCustomerCare