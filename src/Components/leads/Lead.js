import React, { useState, useEffect } from 'react'
import { Link, useHistory } from 'react-router-dom';
import currentDate from '../../helper/dates/currentDate'
import deleteIcon from '../../assets/images/delete.svg';
import editIcon from '../../assets/images/edit.svg';
import Alert from '../layout/Alert'
import PaginatedItems from '../Paginate';
import { baseUrl3 } from '../../helper/constants/constant';
import axios from 'axios';
import $ from 'jquery'

const Lead = () => {

	const history = useHistory()

	var currentMonth = currentDate().currentMonth
	var todayDate = currentDate().currentDate
	var currentYear = currentDate().currentYear
	var date = `${currentMonth} ${todayDate}, ${currentYear}`

	const [first, setfirst] = useState(null)
	const [alert, setAlert] = useState(null)

	const [sortedArray, setSortedArray] = useState([])

	const [search, setSearch] = useState({
		name: "",
		email: "",
		contactNumber: ""
	})
	const [items, setItems] = useState([])

	const getLeads = async () => {
		const res = await fetch(
			`${baseUrl3}/api/v1/leads/getLeads`
		);
		const data = await res.json();
		if (data.status === "success") {
			setItems(data.data)
		} else {
			setAlert("Some error occur.", "danger")
		}
		return data;
	};
	useEffect(() => {
		getLeads()
	}, [])

	const deleteItem = (id) => {
		setfirst(id)
		$('#myModal').modal('show')

	}
	const editItem = (item) => {
		history.push({
			pathname: '/edit-lead',
			state: item
		})

	}


	const searchData = () => {

		var newArray = items.filter((lead) => {
			if (search.name == lead.firstname) {
				return lead
			} if (search.contactNumber == lead.contactnumber) {
				return lead;
			} if (search.email == lead.email) {
				return lead
			}
		});
		if (newArray.length == 0) {
			showAlert("No data found.", "danger")
			console.log("no data found");

		} else {
			setSortedArray(newArray);
			console.log(sortedArray);
		}
	}

	const reset = () => {
		setSortedArray([])
		setSearch({
			name: "",
			email: "",
			contactNumber: ""
		})

	}
	// geting id from paginate.js
	const getId = (id) => {
		setfirst(id)
	};

	const deleteLead = () => {
		const body = {
			id: first
		};
		axios.post(`${baseUrl3}/api/v1/leads/delete`, body)
			.then((response) => {
				console.log(response.data);
				// problem
				if (response.data.status === "success") {
					setTimeout(() => {
						$('#cityDelete').modal('hide')
						showAlert("Lead Deleted successfully.", "success")
					}, 1000);
					// for just mounting the component again 
					//   setdeleteUpdate(new Date)

				} else {
					showAlert("Some error occur.", "danger")
				}
			}
			)

	}

	const showAlert = (message, type) => {
		setAlert({
			message: message,
			type: type
		})
		setTimeout(() => {
			setAlert(null)
		}, 2000);

	}

	return (

		<>

			<div className="dashboard">

				<div className="dashboard-content">

					{/* modal */}

					<div className="modal" id='myModal' tabIndex="-1" style={{ width: "40%", marginLeft: "30%" }}>

						<div className="modal-dialog-centered">

							<div className="modal-content">

								<div className="modal-header">

									<h5 className="modal-title">Are you sure to delete this Lead?</h5>

									<button type="button" className="close" data-dismiss="modal" aria-label="Close">

										<span aria-hidden="true">&times;</span>

									</button>

								</div>

								<div className="modal-footer">

									<button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>

									<button type="button" onClick={deleteLead} className="btn btn-danger">Delete Lead</button>

								</div>

							</div>

						</div>

					</div>

					<header className="title-head d-flex align-items-center mb-5">

						<h1 className="h4">Leads</h1>

						<div className="ml-auto">

							<Link to="/add-Lead" className="btn btn-light icon-right">Add Lead</Link>

							<a href="javascript:void(0);" className="btn btn-light ml-2">{date}  : <span className="text-primary">Today</span>

							</a>

						</div>

					</header>

					<div className="bg-white rounded p-4 mb-4">

						<header className="title-head  mb-4">

							<h4 className="mb-0">Search Lead</h4>

						</header>

						<div className="row">

							<div className="col-md-4 form-group">

								<label htmlFor="">Name</label>

								<input type="text" className="form-control" placeholder="Enter name here"
									value={search.name}
									onChange={(e) => setSearch({ name: (e.target.value) })}
								/>

							</div>

							<div className="col-md-4 form-group">

								<label htmlFor="">Email</label>

								<input type="text" className="form-control" placeholder="Enter email here"
									value={search.email}
									onChange={(e) => setSearch({ email: (e.target.value) })}
								/>

							</div>

							<div className="col-md-4 form-group">

								<label htmlFor="">Contact number</label>

								<input type="text" className="form-control" placeholder="Enter contact number here"
									value={search.contactNumber}
									onChange={(e) => setSearch({ contactNumber: (e.target.value) })}
								/>

							</div>

						</div>

						<div className="row row-gap justify-content-center">

							<div className="col-6 col-lg-2 col-md-3">

								<button type="reset" onClick={reset} className="btn btn-outline-primary btn-block">Reset</button>

							</div>

							<div className="col-6 col-lg-2 col-md-3">

								<button type="submit" onClick={searchData} className="btn btn-primary btn-block">Search</button>

							</div>

						</div>

					</div>

					<Alert alert={alert} />

					<div className="bg-white rounded p-4">

						<header className="title-head mb-4">

							<h4 className="mb-0">Leads</h4>

						</header>

						<div className="table-responsive">

							<table className="table v-aligm-middle medium">

								<thead>

									<tr>

										<th>Name</th>

										<th>Email</th>

										<th>Phone No.</th>

										<th>Location</th>

										<th>Model</th>

										<th>Make</th>

										<th>Status</th>

										<th width="140">Action</th>

									</tr>

								</thead>

								<tbody className="text-muted">

									{

										items.map((item) => (

											<tr key={item.id}>

												<td>{item.name}</td>

												<td>{item.email}</td>

												<td>{item.contactNumber}</td>

												<td>{item.location}</td>

												<td>{item.model}</td>

												<td>{item.make}</td>

												<td>{item.leadStatus}</td>

												<td>
													<button type="button" onClick={() => editItem(item)} className="btn btn-sm btn-light btn-icon pl-2 pr-2" title="Edit"><img src={editIcon} alt="Edit Icon" /></button>
													<button type="button" onClick={() => deleteItem(item.id)} className="btn btn-sm btn-light btn-icon pl-2 pr-2" title="Delete"><img src={deleteIcon} alt="Trash Icon" /></button>


												</td>
											</tr>

										))}

								</tbody>

							</table>

						</div>

					</div>

				</div>

			</div>

		</>

	)
}


export default Lead;
