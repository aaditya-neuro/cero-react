import React, { useState } from 'react'
import AdditionalLeadDetails from './AddLeadStageForm/AdditionalLeadDetails'
import AdditionalVehicleDetails from './AddLeadStageForm/AdditionalVehicleDetails'
import LeadDetails from './AddLeadStageForm/LeadDetails'
import VehicleDetails from './AddLeadStageForm/VehicleDetails'

const AddLead = () => {
  const [page, setPage] = useState(0)
  const formTitles = ["Lead Details", "Vehicle Details", "Additional Vehicle Details", "Additional Lead Details"]
  const [formData, setFormData] = useState({
    //lead details
    salutation: "",
    name: "",
    email: "",
    contactNumber: "",
    location: "",
    isReferral: "",
    referrerName:"",
    referrerContactNumber:"",
    //vehicle details
    vehicleCategory: "",
    vehicleType: "",
    make: "",
    model: "",
    carDetails: "",
    registrationyear: "",
    sellerType: "",
    gstNumber:"",
    // additional vehicle details
    variant: "",
    color: "",
    fuelType: "",
    condition: "",
    cngLpg: "",
    registrationNumber: "",
    yearOfManufacturing: "",
    dateOfRegistration: "",
    engineNumber: "",
    chasisNumber: "",
    rcExpireDate: "",
    isSellerRcOwnerDifferent: "",
    demandedPrice: "",
    // Additional lead details
    leadState: "",
    reasonOfUnqualified:"",
    leadStatus: "",
    leadSource: "",
    comment: "",

  })

  const pageDisplay = () => {

    if (page == 0) {
      return <LeadDetails formData={formData} setFormData={setFormData} />
    }
    else if (page == 1) {
      return <VehicleDetails formData={formData} setFormData={setFormData} />
    }
    else if (page == 2) {
      return <AdditionalVehicleDetails formData={formData} setFormData={setFormData} />
    }
    else {
      return <AdditionalLeadDetails formData={formData} setFormData={setFormData} />
    }
  }


  return (

    <div className="dashboard">

      <div className="dashboard-content">

        <div className="progressbar"></div>

        <div className="col-md-12 form-group d-flex justify-content-between">

          <header className="title-head mb-4 col-md-6">

            <h1 className="h4">{formTitles[page]} </h1>

          </header>

        </div>

        <div className="bg-white rounded p-4">

          <div className="row">

            {pageDisplay()}

          </div>

        </div>

        <div className="row row-gap justify-content-center mt-5 mb-4">

          <div className="col-6 col-lg-2 col-md-3">

            <button className="btn btn-outline-primary btn-block"

              disabled={page == 0}
              onClick={() => {
                setPage((currPage) => currPage - 1)

              }}
            >Prev</button>

          </div>

          <div className="col-6 col-lg-2 col-md-3">

            <button className="btn btn-primary btn-block"

              onClick={() => {
                if (page === formTitles.length - 1) {
                  console.log(formData);
                }
                else {
                  setPage((currPage) => currPage + 1)
                }

              }}>
              {page === formTitles.length - 1 ? "submit" : "Next"}

            </button>

          </div>

        </div>

      </div>

    </div>
  )
}

export default AddLead