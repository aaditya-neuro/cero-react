import React, { useState, useEffect } from 'react'
import { baseUrl2 } from '../../../helper/constants/constant'
import { avoidAlphabets } from '../../../helper/validation/keypressValidation'
import axios from 'axios'
import $ from 'jquery'

const VehicleDetails = ({ formData, setFormData }) => {

  const [vehicleCategory, setVehicleCategory] = useState([])
  const getVehicleCategory = async () => {
    await axios.get(`${baseUrl2}/api/v1/vehicleCategory`)
      .then((response) => {
        if (response.data.status === "success") {
          setVehicleCategory(response.data.data)
        }
      }).catch((error) => {
        console.log(error);
      })
  };

  const [vehicleType, setVehicleType] = useState([])

  const getVehicleType = async () => {
    await axios.get(`${baseUrl2}/api/v1/vehicleType`)
      .then((response) => {
        if (response.data.status === "success") {
          setVehicleType(response.data.data)
        }
      }).catch((error) => {
        console.log(error);
      })
  };

  const [makeData, setMakeData] = useState([])
  const getMakeList = async () => {
    await axios.get(`${baseUrl2}/api/v1/makemodel/make`)
      .then((response) => {
        if (response.data.status === "success") {
          setMakeData(response.data.data)
        }
      }).catch((error) => {
        console.log(error);
      })
  };


  const [modelList, setModelList] = useState([])
  const getmodelList = async () => {
    await axios.get(`${baseUrl2}/api/v1/makemodel/model`)
      .then((response) => {
        if (response.data.status === "success") {
          setModelList(response.data.data)
        }
      }).catch((error) => {
        console.log(error);
      })
  };

  useEffect(() => {
    if(formData.sellerType=="Institutional"){
      $("#institutionalRadio").attr("checked",true)
    }else if(formData.sellerType=="Individual"){
      $("#individualRadio").attr("checked",true)
    }
    getVehicleCategory()
    getVehicleType()
    getMakeList()
    getmodelList()
  }, [])


  const onChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value })
  }

  return (
    <>
      <div className="col-md-6 form-group">

        <label htmlFor="">vehicleCategory<span className="text-red">*</span></label>

        <select type="text" className="form-control" placeholder="Enter vehicle Category here"
          value={formData.vehicleCategory}
          name='vehicleCategory'
          onChange={onChange}
        >

          <option value="">select vehicle Category</option>
          {
            vehicleCategory.map((ele) => {
              return <option key={ele.id} value={ele.id} >{ele.vehicleCategoryName}</option>
            })
          }

        </select>

      </div>

      <div className="col-md-6 form-group">

        <label htmlFor="">vehicle Type<span className="text-red">*</span></label>

        <select type="text" className="form-control" placeholder="Enter vehicle Type  here"
          value={formData.vehicleType}
          onChange={onChange}
          name="vehicleType"
        >
          <option value="">select vehicle Type</option>
          {
            vehicleType.map((ele) => {
              return <option key={ele.id} value={ele.id} >{ele.vehicleTypeName}</option>
            })
          }

        </select>

      </div>

      <div className="col-md-6 form-group">

        <label htmlFor="">Make<span className="text-red">*</span></label>

        <select name="make" id="" className="form-control"
          onChange={onChange}
          value={formData.make}
          data-size="8" title="Select make ">
          <option value="">Select make</option>
          {
            makeData.map((data) => {
              return <option key={data.id} value={data.id}>{data.make}</option>
            })
          }

        </select>

      </div>

      <div className="col-md-6 form-group">

        <label htmlFor="">Model<span className="text-red">*</span></label>

        <select name="model" id="" className="form-control"
          onChange={onChange}
          value={formData.model}
          data-size="8" title="Select model name ">
          <option value="">Select model</option>
          {
            modelList.map((data) => {
              return <option key={data.id} value={data.id}>{data.model}</option>
            })
          }

        </select>

      </div>

      <div className="col-md-6 form-group">

        <label htmlFor=""> Registration year <span className="text-red">*</span></label>

        <input type="text" className="form-control"
          onKeyPress={avoidAlphabets}
          placeholder="Registration Year"
          onChange={onChange}
          value={formData.registrationyear}
          name="registrationyear"
          maxLength={4}
        />

      </div>

      <div className="col-md-6 form-group">

        <label htmlFor=""> Seller Type <span className="text-red">*</span></label>

        <br />

        <div className="custom-control custom-radio custom-control-inline">

          <input type="radio" id="individualRadio" name="sellerType" className="custom-control-input"
            onChange={onChange}
            value="Individual"

          />


          <label className="custom-control-label" for="individualRadio">Individual (direct)</label>

        </div>

        <div className="custom-control custom-radio custom-control-inline">

          <input type="radio" id="institutionalRadio" name="sellerType" className="custom-control-input"
            onChange={onChange}
            value="Institutional"
          />


          <label className="custom-control-label" for="institutionalRadio"> Institutional (indirect)</label>

        </div>

      </div>
      {
        formData.sellerType == "Institutional" ?

          <div className="col-md-6 form-group">

            <label htmlFor="">Gst Number  <span className="text-red">*</span></label>

            <input type="text" className="form-control"
              placeholder="Gst Number"
              onChange={onChange}
              value={formData.gstNumber}
              name="gstNumber"
            />

          </div> : null
      }
      <div className="col-md-6 form-group">
        <label for="exampleFormControlTextarea1">Vehicle Description <span className="text-red">*</span></label>
        <textarea className="form-control" name='carDetails'
          value={formData.carDetails}
          onChange={onChange}
          rows="3"></textarea>
      </div>


    </>
  )
}

export default VehicleDetails