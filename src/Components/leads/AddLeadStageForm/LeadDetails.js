import React, { useState, useEffect } from 'react'
import { avoidAlphabets, avoidSpace } from '../../../helper/validation/keypressValidation'
import { baseUrl2 } from '../../../helper/constants/constant'
import axios from 'axios'
import $ from 'jquery'


const LeadDetails = ({ formData, setFormData }) => {

    const [state, setState] = useState([])
    const [stateId, setStateId] = useState()

    const getState = async () => {
        await axios.get(`${baseUrl2}/api/v1/location/state`)
            .then((response) => {
                if (response.data.status === "success") {
                    setState(response.data.data)
                }
            }).catch((error) => {
                console.log(error);
            })
    }

    const [city, setCity] = useState([])

    const getCity = async () => {
        const body = {
            stateId: stateId
        }
        await axios.post(`${baseUrl2}/api/v1/location/city/getByState`, body)
            .then((response) => {
                if (response.data.status === "success") {
                    setCity(response.data.data)
                }
            }).catch((error) => {
                console.log(error);
            })
    }


    useEffect(() => {
        getState()
        if (stateId) {
            getCity()
        }
        if(formData.referrerContactNumber||formData.referrerName){
            $("#referralSwitch").attr("checked",true)
            setisReferral(true)
        }

    }, [stateId])


    const [isReferral, setisReferral] = useState(false)
    formData.isReferral = isReferral

    const checkeRferral = () => {
        isReferral == false ?
            setisReferral(true) :
            setisReferral(false)
    }

    const onChange = (e) => {
        setFormData({ ...formData, [e.target.name]: e.target.value })
    }


    return (
        <>
            <div className="col-md-6 form-group">

                <label for="exampleFormControlSelect1">Title  <span className="text-red">*</span></label>

                <select className="form-control" id="exampleFormControlSelect1"
                    onChange={onChange}
                    value={formData.salutation}
                    name="salutation"
                >
                    <option>select Title</option>

                    <option value='Mr.'>Mr</option>

                    <option value='Mrs.'>Mrs</option>

                    <option value='Ms'>Ms</option>

                </select>

            </div>

            <div className="col-md-6 form-group">

                <label htmlFor=""> Name <span className="text-red">*</span></label>

                <input type="text" className="form-control"
                    placeholder="Full Name"
                    onChange={onChange}
                    value={formData.name}
                    name="name"
                />

            </div>

            <div className="col-md-6 form-group">

                <label htmlFor="">Email<span className="text-red">*</span></label>

                <input type="text" className="form-control"
                    onKeyPress={avoidSpace}
                    placeholder="Email"
                    onChange={onChange}
                    value={formData.email}
                    name="email"
                />

            </div>

            <div className="col-md-6 form-group">

                <label htmlFor="">Contact Number<span className="text-red">*</span></label>

                <input type="text" className="form-control"
                    onKeyPress={avoidAlphabets}
                    placeholder="Contact Number"
                    onChange={onChange}
                    value={formData.contactNumber}
                    name="contactNumber"
                    maxLength={10}
                />


            </div>

            <div className="col-md-6 form-group">

                <label htmlFor="">State<span className="text-red">*</span></label>

                <select name="stateId" id="" className="form-control"
                    value={stateId}
                    onChange={(e) => setStateId(e.target.value)}
                    data-size="8" title="Select State ">
                    <option value="">Select State</option>
                    {
                        state.map((data) => {
                            return <option key={data.id} value={data.id}>{data.name}</option>
                        })
                    }

                </select>

            </div>

            <div className="col-md-6 form-group">

                <label htmlFor="">City<span className="text-red">*</span></label>

                <select name="location" id="" className="form-control"
                    onChange={onChange}
                    data-size="8" title="Select State ">
                    <option value="">Select City</option>
                    {
                        city.map((data) => {
                            return <option key={data.id} value={data.id}>{data.name}</option>
                        })
                    }

                </select>

            </div>

            <div className="col-md-2 form-group">

                <br />

                <div className="custom-control custom-switch">

                    <input type="checkbox" className="custom-control-input" id='referralSwitch'
                        onClick={checkeRferral}

                    />

                    <label className="custom-control-label" htmlFor="referralSwitch">Is referral ? </label>

                </div>

            </div>


            {

                isReferral == true ?

                    <>

                        <div className="col-md-4 form-group">

                            <label htmlFor="">Referrer name <span className="text-red">*</span></label>

                            <input type="text" className="form-control"
                                placeholder="Referrer Name"
                                onChange={onChange}
                                value={formData.referrerName}
                                name="referrerName"
                            />

                        </div>

                        <div className="col-md-6 form-group">

                            <label htmlFor=""> Referrer Contact Number<span className="text-red">*</span></label>

                            <input type="text" className="form-control"
                                onKeyPress={avoidAlphabets}
                                placeholder="Contact Number"
                                onChange={onChange}
                                value={formData.referrerContactNumber}
                                name="referrerContactNumber"
                                maxLength={10}
                            />

                        </div>
                    </>
                    : null

            }

        </>
    )
}

export default LeadDetails