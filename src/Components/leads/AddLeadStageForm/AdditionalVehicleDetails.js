import React,{useEffect} from 'react'
import { avoidAlphabets } from '../../../helper/validation/keypressValidation'
import $ from 'jquery'

const AdditionalVehicleDetails = ({ formData, setFormData }) => {
  useEffect(() => {
    if(formData.cngLpg=="Yes"){
      $("#cngLpgYes").attr("checked",true)
    }
    else if(formData.cngLpg=="No"){
      $("#cngLpgNo").attr("checked",true)
    }
    if(formData.isSellerRcOwnerDifferent=="Yes"){
      $("#isSellerRcOwnerDifferentYes").attr("checked",true)
    }
    else if(formData.isSellerRcOwnerDifferent=="No"){
      $("#isSellerRcOwnerDifferentNo").attr("checked",true)
    }
  }, [])
  
  const onChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value })
  }
  return (
    <>

      <div className="col-md-4 form-group">

        <label htmlFor="">Year Of Manufacturing <span className="text-red">*</span></label>

        <input type="text" className="form-control"
          placeholder="Enter Year Of Manufacturing Here"
          onKeyPress={avoidAlphabets}
          onChange={onChange}
          value={formData.yearOfManufacturing}
          name="yearOfManufacturing"
          maxLength={4}
        />

      </div>

      <div className="col-md-4 form-group">

        <label htmlFor="">Date Of Registration <span className="text-red">*</span></label>

        <input type="date" className="form-control"
          placeholder="Enter Date Of Registration Here"
          onChange={onChange}
          value={formData.dateOfRegistration}
          name="dateOfRegistration"

        />

      </div>

      <div className="col-md-4 form-group">

        <label htmlFor="">R.C Expire Date <span className="text-red">*</span></label>

        <input type="date" className="form-control"
          placeholder="Enter R.C Expire Date Here"
          onChange={onChange}
          value={formData.rcExpireDate}
          name="rcExpireDate"

        />

      </div>

      <div className="col-md-4 form-group">

        <label htmlFor="">Engine Number <span className="text-red">*</span></label>

        <input type="text" className="form-control"
          placeholder="Enter Engine Number Here"
          onKeyPress={avoidAlphabets}
          onChange={onChange}
          value={formData.engineNumber}
          name="engineNumber"
        />

      </div>

      <div className="col-md-4 form-group">

        <label htmlFor="">Chasis Number <span className="text-red">*</span></label>

        <input type="text" className="form-control"
          placeholder="Enter Chasis Number Here"
          onKeyPress={avoidAlphabets}
          onChange={onChange}
          value={formData.chasisNumber}
          name="chasisNumber"
        />

      </div>

      <div className="col-md-4 form-group">

        <label htmlFor="">Demanded Amount <span className="text-red">*</span></label>

        <input type="text" className="form-control"
          placeholder="Enter Demanded Amount Here"
          onKeyPress={avoidAlphabets}
          onChange={onChange}
          value={formData.demandedPrice}
          name="demandedPrice"
        />

      </div>

      <div className="col-md-4 form-group">

        <label htmlFor="">Vehicle Colour  <span className="text-red">*</span></label>

        <input type="text" className="form-control"
          placeholder="Enter Colour Here"
          onChange={onChange}
          value={formData.color}
          name="color"
        />

      </div>

      <div className="col-md-4 form-group">

        <label for="exampleFormControlSelect1">Fuel Type  <span className="text-red">*</span></label>

        <select className="form-control" id="exampleFormControlSelect1"
          onChange={onChange}
          value={formData.fuelType}
          name="fuelType"
        >
          <option>select Fuel Type</option>

          <option value='Petrol'>Petrol</option>

          <option value='Diesel'>Diesel</option>

          <option value='Electric'>Electric</option>

          <option value='Hybrid'>Hybrid</option>

        </select>

      </div>

      <div className="col-md-4 form-group">

        <label for="exampleFormControlSelect1">Condition Rating <span className="text-red">*</span></label>

        <select className="form-control" id="exampleFormControlSelect1"
          onChange={onChange}
          value={formData.condition}
          name="condition"
        >
          <option>select Condition Rating</option>

          <option value='1'>1</option>

          <option value='2'>2</option>

          <option value='3'>3</option>

          <option value='4'>4</option>

          <option value='5'>5</option>

        </select>

      </div>

      <div className="col-md-2 form-group">

        <label htmlFor="">CNG/LPG <span className="text-red">*</span></label>

        <br />

        <div className="custom-control custom-radio custom-control-inline">

          <input type="radio" id="cngLpgYes" name="cngLpg" className="custom-control-input"
            onChange={onChange}
            value="Yes"

          />

          <label className="custom-control-label" for="cngLpgYes">Yes</label>

        </div>

        <div className="custom-control custom-radio custom-control-inline">

          <input type="radio" id="cngLpgNo" name="cngLpg" className="custom-control-input"
            onChange={onChange}
            value="No"
          />

          <label className="custom-control-label" for="cngLpgNo">No</label>

        </div>

      </div>

      <div className="col-md-2 form-group">

        <label htmlFor="">Is Seller & Owner Different <span className="text-red">*</span></label>

        <br />

        <div className="custom-control custom-radio custom-control-inline">

          <input type="radio" id="isSellerRcOwnerDifferentYes" name="isSellerRcOwnerDifferent" className="custom-control-input"
            onChange={onChange}
            value="Yes"

          />

          <label className="custom-control-label" for="isSellerRcOwnerDifferentYes">Yes</label>

        </div>

        <div className="custom-control custom-radio custom-control-inline">

          <input type="radio" id="isSellerRcOwnerDifferentNo" name="isSellerRcOwnerDifferent" className="custom-control-input"
            onChange={onChange}
            value="No"
          />

          <label className="custom-control-label" for="isSellerRcOwnerDifferentNo">No</label>

        </div>

      </div>

      <div className="col-md-4 form-group">

        <label htmlFor="">Registration Number <span className="text-red">*</span></label>

        <input type="text" className="form-control"
          placeholder="Enter Registration Number Here"
          onChange={onChange}
          value={formData.registrationNumber}
          name="registrationNumber"
        />

      </div>

      <div className="col-md-4 form-group">
        <label for="exampleFormControlTextarea1">Variant <span className="text-red">*</span></label>
        <textarea className="form-control" name='variant'
          value={formData.variant}
          onChange={onChange}
          rows="3"></textarea>
      </div>



    </>
  )
}

export default AdditionalVehicleDetails