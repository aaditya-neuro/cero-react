import React from 'react'

const AdditionalLeadDetails = ({ formData, setFormData }) => {

  const onChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value })
  }

  return (
    <>
      <div className="col-md-6 form-group">

        <label for="exampleFormControlSelect1">Lead Status  <span className="text-red">*</span></label>

        <select className="form-control" id="exampleFormControlSelect1"
          onChange={onChange}
          value={formData.leadStatus}
          name="leadStatus"
        >
          <option>select Lead Status</option>

          <option value='Qualified'>Qualified</option>

          <option value='unQualified'>Un Qualified</option>


        </select>

      </div>

      {
        formData.leadStatus == "unQualified" ?

          <div className="col-md-6 form-group">

            <label for="exampleFormControlSelect1">Reason For Unqualified Lead  <span className="text-red">*</span></label>

            <select className="form-control"
              onChange={onChange}
              value={formData.reasonOfUnqualified}
              name="reasonOfUnqualified"
            >
              <option>select Reason</option>

              <option value='junkLead'>Junk Lead</option>

              <option value='Garbage'>Garbage</option>

              <option value='fakeCall'>Fake Call</option>

              <option value='UnServiceable'>Un Serviceable</option>


            </select>

          </div> : null

      }

      <div className="col-md-6 form-group">

        <label for="exampleFormControlSelect1">Lead State  <span className="text-red">*</span></label>

        <select className="form-control" id="exampleFormControlSelect1"
          onChange={onChange}
          value={formData.leadState}
          name="leadState"
        >
          <option>select Lead State</option>

          <option value='Hot'>Hot</option>

          <option value='Warm'>Warm</option>

          <option value='Cold'>Cold</option>

        </select>

      </div>



      <div className="col-md-6 form-group">

        <label for="exampleFormControlSelect1">Lead Source  <span className="text-red">*</span></label>

        <select className="form-control" id="exampleFormControlSelect1"
          onChange={onChange}
          value={formData.leadSource}
          name="leadSource"
        >
          <option>select Lead Source</option>

          <option value='Website'>Website</option>

          <option value='Call'>Call</option>

          <option value='Chat'>Chat</option>

          <option value='E-mail'>E-mail</option>


        </select>

      </div>



      <div className="col-md-6 form-group">

        <label for="exampleFormControlTextarea1">Comment <span className="text-red">*</span></label>
        <textarea className="form-control" name='comment'
          value={formData.comment}
          onChange={onChange}
          rows="3">

        </textarea>

      </div>

    </>
  )
}

export default AdditionalLeadDetails