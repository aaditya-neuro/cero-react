import React, { useState, useEffect } from 'react'
import { baseUrl2 } from '../../../helper/constants/constant'
import axios from 'axios'
import { useHistory } from 'react-router-dom'
import Alert from '../../layout/Alert'
import { avoidAlphabets } from '../../../helper/validation/keypressValidation'
import $ from 'jquery'
import UnauthorisedModal from '../../layout/UnauthorisedModal'

const EditModel = (props) => {
  const previousData = props.location.state

  const history = useHistory()
  const [data, setData] = useState({
    makeId: '',
    maxWeight: '',
    uom: '',
    oldMaterialNumber: '',
    model: '',
    materialDesc: '',
    vehicleCategoryId: ''
  })

  const [makeData, setMakeData] = useState([])
  const getMakeList = async () => {
    const res = await fetch(
      `${baseUrl2}/api/v1/makemodel/make`
    );
    const data = await res.json();
    if (data.status === "success") {
      setMakeData(data.data)
    } else {
      setAlert("Some error occur.", "danger")
    }
    return data;
  };

  const [vehicleCategory, setVehicleCategory] = useState([])
  const getVehicleType = async () => {
    const res = await fetch(
      `${baseUrl2}/api/v1/vehicleCategory`
    );
    const data = await res.json();
    if (data.status === "success") {
      setVehicleCategory(data.data)
    } else {
      setAlert("Some error occur.", "danger")
    }
    return data;
  };

  useEffect(() => {
    getMakeList()
    getVehicleType()
    if (!previousData) {
      $("#unauthorisedUser").modal('show')
    } else {
      setData({
        makeId: previousData.make_id,
        maxWeight: previousData.max_weight,
        uom: previousData.uom,
        oldMaterialNumber: previousData.old_material_number,
        model: previousData.model,
        materialDesc: previousData.material_desc,
        vehicleCategoryId: previousData.vehicle_category_id
      })
    }

  }, [])


  const [error, setError] = useState({})
  const [isSubmit, setIsSubmit] = useState(false)
  const [alert, setAlert] = useState(null)

  const validation = (values) => {
    const formErrors = {}
    if (!values.makeId) {
      formErrors.makeId = "Please select make."
    }
    if (!values.maxWeight) {
      formErrors.maxWeight = "Please add  max Weight."
    }
    if (!values.uom) {
      formErrors.uom = "Please add Uom."
    }
    if (!values.oldMaterialNumber) {
      formErrors.oldMaterialNumber = "Please add old Material Number."
    }
    if (!values.model) {
      formErrors.model = "Please add Model."
    }
    if (!values.materialDesc) {
      formErrors.materialDesc = "Please add material Description."
    }
    if (!values.vehicleCategoryId) {
      formErrors.vehicleCategoryId = "Please add vehicle Category."
    }
    return formErrors
  }


  const onChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setError({})
    setIsSubmit(false)
  }
  const reset = () => {
    setData({
      makeId: '',
      maxWeight: '',
      uom: '',
      oldMaterialNumber: '',
      model: '',
      materialDesc: '',
      vehicleCategoryId: ''
    })
  }
  const handleSubmit = () => {
    setError(validation(data))
    setIsSubmit(true)
  }

  const showAlert = (message, type) => {
    setAlert({
      message: message,
      type: type
    })
    setTimeout(() => {
      setAlert(null)
    }, 2000);
  }

  useEffect(() => {
    if (Object.keys(error).length == 0 && isSubmit) {
      console.log(data);
      const body = {
        ...data
      };
      axios.post(`${baseUrl2}/api/v1/makemodel/model/create`, body)
        .then((response) => {
          console.log(response.data);
          if (response.data.status === "success") {
            showAlert("plant added successfully.", "success")
            setData({
              makeId: '',
              maxWeight: '',
              uom: '',
              oldMaterialNumber: '',
              model: '',
              materialDesc: '',
              vehicleCategoryId: ''
            })
            setTimeout(() => {
              history.push('/Model')

            }, 2000);

          } else {
            showAlert("Some error occur.", "danger")
          }

        }).catch((error) => {
          showAlert("some error occured", "danger")
        })
    }
  }, [error])
  return (
    <div className="dashboard">

      <div className="dashboard-content">
       
       <UnauthorisedModal/>

        <div className="col-md-12 form-group d-flex justify-content-between">

          <header className="title-head mb-4 col-md-6">

            <h1 className="h4">Edit Model </h1>

          </header>

        </div>

        <Alert alert={alert} />

        <div className="bg-white rounded p-4">

          <div className="row">

            <div className="col-md-6 form-group">

              <label htmlFor="">Make<span className="text-red">*</span></label>

              <select name="makeId" id="" className="form-control"
                onChange={onChange}
                value={data.makeId}
                data-size="8" title="Select make ">
                <option value="">Select make</option>
                {
                  makeData.map((data) => {
                    return <option key={data.id} value={data.id}>{data.make}</option>
                  })
                }

              </select>

              <p className="text-red">{error.makeId}</p>

            </div>

            <div className="col-md-6 form-group">

              <label htmlFor="">Vehicle Category<span className="text-red">*</span></label>

              <select name="vehicleCategoryId" id="" className="form-control"
                onChange={onChange}
                value={data.vehicleCategoryId}
                data-size="8" title="Select make ">

                <option value="">Select vehicle Category</option>

                {
                  vehicleCategory.map((data) => {
                    return <option key={data.id} value={data.id}>{data.vehicleCategoryName}</option>
                  })
                }

              </select>

              <p className="text-red">{error.vehicleCategoryId}</p>

            </div>

            <div className="col-md-6 form-group">

              <label htmlFor=""> max Weight <span className="text-red">*</span></label>

              <input type="text" className="form-control"
                placeholder="Enter weight here in kg"
                name='maxWeight'
                onChange={onChange}
                value={data.maxWeight}
                onKeyPress={avoidAlphabets}
                maxLength={5}

              />

              <p className="text-red">{error.maxWeight}</p>

            </div>

            <div className="col-md-6 form-group">

              <label htmlFor="">UOM<span className="text-red">*</span></label>
              <input type="text" className="form-control"
                placeholder="Enter uom  here"
                name='uom'
                onChange={onChange}
                value={data.uom}

              />

              <p className="text-red">{error.uom}</p>

            </div>

            <div className="col-md-6 form-group">

              <label htmlFor="">old Material Number<span className="text-red">*</span></label>
              <input type="text" className="form-control"
                placeholder="Enter oldnMaterial Number  here"
                name='oldMaterialNumber'
                onChange={onChange}
                value={data.oldMaterialNumber}

              />

              <p className="text-red">{error.oldMaterialNumber}</p>

            </div>

            <div className="col-md-6 form-group">

              <label htmlFor="">material Description<span className="text-red">*</span></label>
              <input type="text" className="form-control"
                placeholder="Enter material Description here"
                name='materialDesc'
                onChange={onChange}
                value={data.materialDesc}

              />

              <p className="text-red">{error.uom}</p>

            </div>

            <div className="col-md-6 form-group">

              <label htmlFor="">Model<span className="text-red">*</span></label>
              <input type="text" className="form-control"
                placeholder="Enter Model here"
                name='model'
                onChange={onChange}
                value={data.model}

              />

              <p className="text-red">{error.model}</p>

            </div>

          </div>

          <div className="row row-gap justify-content-center mt-5 mb-4">

            <div className="col-6 col-lg-2 col-md-3">

              <button type="reset" className="btn btn-outline-primary btn-block" onClick={reset}>Reset</button>

            </div>

            <div className="col-6 col-lg-2 col-md-3">

              <button type="submit" className="btn btn-primary btn-block" onClick={handleSubmit} >Submit</button>

            </div>

          </div>

        </div>

      </div>

    </div>
  )
}

export default EditModel