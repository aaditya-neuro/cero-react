
import React, { useState, useEffect } from 'react'
import { Link, useHistory } from 'react-router-dom'

import ReactPaginate from 'react-paginate';
import currentDate from '../../../helper/dates/currentDate'
import deleteIcon from '../../../assets/images/delete.svg';
import editIcon from '../../../assets/images/edit.svg';
import { baseUrl2 } from '../../../helper/constants/constant';
import axios from 'axios';
import $ from 'jquery'
import Alert from '../../layout/Alert';
const VehicleCategory = () => {

    var currentMonth = currentDate().currentMonth
    var todayDate = currentDate().currentDate
    var currentYear = currentDate().currentYear

    var date = `${currentMonth} ${todayDate}, ${currentYear}`

    const [filterData, setFilterData] = useState({
        name: "",
    })

    const onchange = (e) => {
        setFilterData({ ...filterData, [e.target.name]: e.target.value })
        setError('')
    }

    const [error, setError] = useState(null)
    const search = () => {
        if (filterData.name || filterData.state || filterData.city) {
            console.log(filterData);
            setFilterData({
                name: "",
            })

        } else {
            setError("please fill one of these fields.")
        }

    }

    const reset = () => {
        setFilterData({
            name: "",
        })
        setError('')
    }


    const history = useHistory()

    const editPlant = (ele) => {

        history.push({
            pathname: '/edit-vehicleCategory',
            state: ele
        })
    }

    const [vehicleCategoryDeleteId, setVehicleCategoryDeleteId] = useState()
    const Delete = (id) => {
        $('#vehicleCategoryDelete').modal('show')
        setVehicleCategoryDeleteId(id)

    }
    const [deleteUpdate, setdeleteUpdate] = useState()
    const deleteVehicleCategory = () => {
        console.log(vehicleCategoryDeleteId);
        //api call here to delete vehicleCategory
        const body = {
            vehicleCategoryId: vehicleCategoryDeleteId
        };

        axios.post(`${baseUrl2}/api/v1/vehicleCategory/delete`, body)
            .then((response) => {
                console.log(response.data);
                // problem
                if (response.data.status === "success") {
                    setTimeout(() => {
                        $('#vehicleCategoryDelete').modal('hide')
                        showAlert("Vehicle Category Deleted successfully.", "success")
                    }, 1000);
                    // for just mounting the component again 
                    setdeleteUpdate(new Date)

                } else {
                    showAlert("Some error occur.", "danger")
                }
            }
            )
    }
    // for alerts 
    const [alert, setAlert] = useState(null)

    const showAlert = (message, type) => {
        setAlert({
            message: message,
            type: type
        })
        setTimeout(() => {
            setAlert(null)
        }, 2000);

    }
    const [items, setItems] = useState([])
    const getVehicleCategory = async () => {
        const res = await fetch(
            `${baseUrl2}/api/v1/vehicleCategory`
        );
        const data = await res.json();
        if (data.status === "success") {
            setItems(data.data)
        } else {
            setAlert("Some error occur.", "danger")
        }
        return data;
    };

    useEffect(() => {
        getVehicleCategory()
        setError('')
    }, [deleteUpdate])


    // pagination
    //   const [items, setItems] = useState([])
    //   const [pageCount, setPageCount] = useState(0)
    //   const [currentPageShow, setCurrentPageShow] = useState(1)
    //   let limit = 5

    //   // run first time to get data from api 
    //   useEffect(() => {
    //     const getComments = async () => {
    //       const res = await fetch(
    //         `http://localhost:3004/comments?_page=1&_limit=${limit}`
    //       )
    //       const data = await res.json();
    //       const total = res.headers.get('x-total-count')
    //       setPageCount(Math.ceil(total/limit))
    //       setItems(data);
    //     }
    //     getComments()
    //   }, [])

    // // run when page changes 

    //   const fetchComments = async (currentPage) => {
    //     const res = await fetch(
    //       `http://localhost:3004/comments?_page=${currentPage}&_limit=${limit}`
    //     );
    //     const data = await res.json();
    //     return data;
    //   };
    // // function to trigger page change in pagination
    //   const handlePageChange = async(data) => {
    //      let currentPage= data.selected +1
    //      setCurrentPageShow(currentPage)
    //    const commentsFromServer = await fetchComments(currentPage)
    //    setItems(commentsFromServer)

    //   }

    return (
        <div className="dashboard">

            <div className="dashboard-content">

                <div className="modal" id='vehicleCategoryDelete' tabIndex="-1" style={{ width: "40%", marginLeft: "30%" }}>

                    <div className="modal-dialog-centered">

                        <div className="modal-content">

                            <div className="modal-header">

                                <h5 className="modal-title">Are you sure to delete vehicle Category?</h5>

                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">

                                    <span aria-hidden="true">&times;</span>

                                </button>

                            </div>

                            <div className="modal-footer">

                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>

                                <button type="button" onClick={deleteVehicleCategory} className="btn btn-danger">Delete Plant</button>

                            </div>

                        </div>

                    </div>

                </div>


                <header className="title-head d-flex align-items-center mb-5">

                    <h1 className="h4">Vehicle Category</h1>

                    <div className="ml-auto">

                        <Link to="/add-vehicleCategory" className="btn btn-light icon-right">Add Vehicle Category <img src="images/add-user.svg" alt="" /></Link>

                        <Link to="javascript:void(0);" className="btn btn-light ml-2">{date} : <span className="text-primary">Today</span>

                        </Link>

                    </div>

                </header>

                <Alert alert={alert} />

                <div className="bg-white rounded p-4 mb-4">

                    <header className="title-head  mb-4">

                        <h4 className="mb-0">Search Vehicle Category</h4>

                    </header>

                    <div className="row">

                        <div className="col-md-4 form-group">

                            <label for="">Name</label>

                            <input type="text" className="form-control" placeholder="Enter name here"
                                name='name'
                                onChange={onchange}
                                value={filterData.name}
                            />
                             <p className='text-red'>{error}</p>

                        </div>


                    </div>

                    <div className="row row-gap justify-content-center">

                        <div className="col-6 col-lg-2 col-md-3">

                            <button type="reset" className="btn btn-outline-primary btn-block" onClick={reset}>Reset</button>

                        </div>

                        <div className="col-6 col-lg-2 col-md-3">

                            <button type="submit" className="btn btn-primary btn-block" onClick={search}>Search</button>

                        </div>

                    </div>

                </div>

                <div className="bg-white rounded p-4">

                    <header className="title-head mb-4">

                        <h4 className="mb-0">Vehicle Category's Data</h4>

                    </header>

                    <div className="table-responsive">

                        <table className="table v-aligm-middle medium">

                            <thead>

                                <tr>

                                    <th>Vehicle Category</th>

                                    <th>Status</th>

                                    <th width="140">Action</th>

                                </tr>

                            </thead>

                            <tbody className="text-muted">
                                {
                                    items.map((ele) => {
                                        return (
                                            <tr key={ele.id}>
                                                <td>{ele.vehicleCategoryName}</td>
                                                <td>{ele.status}</td>

                                                <td>
                                                    <button type="button" onClick={() => editPlant(ele)} className="btn btn-sm btn-light btn-icon pl-2 pr-2" title="Edit"><img src={editIcon} alt="Edit Icon" /></button>
                                                    <button type="button" onClick={() => Delete(ele.id)} className="btn btn-sm btn-light btn-icon pl-2 pr-2" title="Delete"><img src={deleteIcon} alt="Trash Icon" /></button>
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                            </tbody>

                        </table>

                    </div>

                </div>

                {/* <div className="d-flex mt-5">

          <div className="list-number">Page {currentPageShow}  of {pageCount}</div>

          <nav aria-label="Table navigation" className="w-100">

            <ul className="pagination justify-content-center">

              <ReactPaginate
                previousLabel={'previous'}
                nextLabel={"next"}
                breakLabel={'....'}
                pageCount={pageCount}
                marginPagesDisplayed={2}
                pageRangeDisplayed={2}
                onPageChange={handlePageChange}
                containerClassName={'pagination justify-content-center'}
                pageClassName={'page-item'}
                pageLinkClassName={'page-link'}
                previousClassName={'page-item'}
                nextClassName={'page-item'}
                previousLinkClassName={'page-link'}
                nextLinkClassName={'page-link'}
                breakLinkClassName={'page-link'}
                activeClassName={'active'}

              />

            </ul>

          </nav>

        </div> */}

            </div>

        </div>
    )
}

export default VehicleCategory