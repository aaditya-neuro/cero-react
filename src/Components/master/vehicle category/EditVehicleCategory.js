import React, { useState, useEffect } from 'react'
import Alert from '../../layout/Alert'
import validation from '../../../helper/validation/add_vehicleType_validation'
import { baseUrl2 } from '../../../helper/constants/constant'
import axios from 'axios'
import { useHistory } from 'react-router-dom'
import $ from 'jquery'
import UnauthorisedModal from '../../layout/UnauthorisedModal'

const EditVehicleCategory = (props) => {

    const previousData = props.location.state
    const history = useHistory()
  
    const [error, seterror] = useState({})
    const [isSubmit, setIsSubmit] = useState(false)
    const [alert, setAlert] = useState(null)

    useEffect(() => {
        if (!previousData) {
            $("#unauthorisedUser").modal('show')
        } else {
            if (previousData.status === 1) {
                $("#vehicleCategoryStatusActive").attr("checked", true)
            } else {
                $("#vehicleCategoryStatusInActive").attr("checked", true)
            }
            setData({
                vehicleCategory: previousData.vehicleCategoryName,
                status: previousData.status
            })
        }
    }, [])


    const [data, setData] = useState({
        vehicleCategory: "",
        status: ""
    })
    const onchange = (e) => {
        setData({ ...data, [e.target.name]: e.target.value })
        seterror({})
        setIsSubmit(false)
    }
    const onStatusChange = (e) => {
        setData({ ...data, [e.target.name]: e.target.value })
        seterror({})
        setIsSubmit(false)
    }
    const showAlert = (message, type) => {
        setAlert({
            message: message,
            type: type
        })
        setTimeout(() => {
            setAlert(null)
        }, 2000);
    }

    const reset = () => {
        setData({
            vehicleCategory: "",
            status: ""
        })
    }
    const handleSubmit = () => {
        seterror(validation(data))
        setIsSubmit(true)
    }
    useEffect(() => {
        if (Object.keys(error).length == 1 && isSubmit) {
            console.log(data);
            const body = {
                "vehicleCategoryId": previousData.id,
                "vehicleCategoryName": data.vehicleCategory,
                "status": JSON.parse(data.status)
            }

            axios.post(`${baseUrl2}/api/v1/vehicleCategory/update`, body)
                .then((response) => {
                    console.log(response.data);
                    if (response.data.status === "success") {
                        showAlert("Vehicle Category edited successfully.", "success")
                        setData({
                            vehicleCategory: "",
                            status: ""
                        })
                        setTimeout(() => {
                            history.push('/vehicleCategory')

                        }, 2000);

                    } else {
                        showAlert("Some error occur.", "danger")
                    }

                }).catch((error)=>{
                    showAlert("Some error occur.", "danger")
                })
        }
    }, [error])

    return (

        <div className="dashboard">

            <div className="dashboard-content">

               <UnauthorisedModal/>

                <div className="col-md-12 form-group d-flex justify-content-between">

                    <header className="title-head mb-4 col-md-6">

                        <h1 className="h4">Edit VehicleCategory</h1>

                    </header>

                </div>

                <Alert alert={alert} />

                <div className="bg-white rounded p-4">

                    <div className="row">

                        <div className="col-md-6 form-group">

                            <label htmlFor="">Vehicle Category<span className="text-red">*</span></label>

                            <input type="text" className="form-control"
                                placeholder="Enter vehicle category name here"
                                name='vehicleCategory'
                                onChange={onchange}
                                value={data.vehicleCategory}

                            />

                            <p className="text-red">{error.vehicleCategory}</p>

                        </div>

                        <div className="col-md-6 form-group">

                            <label htmlFor="">Status<span className="text-red">*</span></label>

                            <div className="form-check d-flex">

                                <input className="form-check-input" type="radio"
                                    name="status"
                                    id="vehicleCategoryStatusActive"
                                    onChange={onStatusChange}
                                    value={1}
                                />

                                <label className="form-check-label" for="exampleRadios1">
                                    Active
                                </label>

                                <div className="form-check mx-2">

                                    <input className="form-check-input" type="radio"
                                        name="status"
                                        id="vehicleCategoryStatusInActive"
                                        onChange={onStatusChange}
                                        value={0} />

                                    <label className="form-check-label" for="exampleRadios2">
                                        Inactive
                                    </label>

                                </div>

                            </div>

                        </div>

                        <p className="text-red">{error.status}</p>

                    </div>

                    <div className="row row-gap justify-content-center mt-5 mb-4">

                        <div className="col-6 col-lg-2 col-md-3">

                            <button type="reset" className="btn btn-outline-primary btn-block" onClick={reset}>Reset</button>

                        </div>

                        <div className="col-6 col-lg-2 col-md-3">

                            <button type="submit" className="btn btn-primary btn-block" onClick={handleSubmit} >Submit</button>

                        </div>

                    </div>

                </div>

            </div>

        </div>
    )
}

export default EditVehicleCategory