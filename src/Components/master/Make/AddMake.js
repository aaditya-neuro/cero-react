import React, { useState,useEffect } from 'react'
import { baseUrl2 } from '../../../helper/constants/constant'
import axios from 'axios'
import { useHistory } from 'react-router-dom'
import Alert from '../../layout/Alert'


const AddMake = () => {
  const history = useHistory
  const [data, setData] = useState({
    makeName: '',
    makeCode: ''
  })

  const [error, setError] = useState({})
  const [isSubmit, setIsSubmit] = useState(false)
  const [alert, setAlert] = useState(null)

  const validation = (values) => {
    const formErrors = {}
    if (!values.makeName) {
      formErrors.makeName = "Please add Make name."
    }
    if (!values.makeCode) {
      formErrors.makeCode = "Please add Make code."
    }
    return formErrors
  }


  const onChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setError({})
    setIsSubmit(false)
  }
  const reset = () => {
    setData({
      makeName: '',
      makeCode: ''
    })
  }
  const handleSubmit = () => {
    setError(validation(data))
    setIsSubmit(true)
  }

  const showAlert = (message, type) => {
    setAlert({
      message: message,
      type: type
    })
    setTimeout(() => {
      setAlert(null)
    }, 2000);
  }

  useEffect(() => {
    if (Object.keys(error).length == 0 && isSubmit) {
      console.log(data);
      const body = {
        "makeName":data.makeName,
        "makeSAPCode":data.makeCode,
      };
      axios.post(`${baseUrl2}/api/v1/makemodel/make/create`, body)
        .then((response) => {
          console.log(response.data);
          if (response.data.status === "success") {
            showAlert("Make added successfully.", "success")
            setData({
              makeName: '',
              makeCode: ''
            })
            setTimeout(() => {
              history.push('/Make')

            }, 2000);

          } else {
            showAlert("Some error occur.", "danger")
          }

        }).catch((error)=>{
          showAlert("some error occured", "danger")
        })
    }
  }, [error])


  return (

    <div className="dashboard">

      <div className="dashboard-content">

        <div className="col-md-12 form-group d-flex justify-content-between">

          <header className="title-head mb-4 col-md-6">

            <h1 className="h4">Add Make </h1>

          </header>

        </div>

        <Alert alert={alert}/>

        <div className="bg-white rounded p-4">

          <div className="row">

            <div className="col-md-6 form-group">

              <label htmlFor=""> Make <span className="text-red">*</span></label>

              <input type="text" className="form-control"
                placeholder="Enter make here"
                name='makeName'
                onChange={onChange}
                value={data.makeName}

              />


              <p className="text-red">{error.makeName}</p>

            </div>

            <div className="col-md-6 form-group">

              <label htmlFor="">Make SAP Code<span className="text-red">*</span></label>
              <input type="text" className="form-control"
                placeholder="Enter make code here"
                name='makeCode'
                onChange={onChange}
                value={data.makeCode}
                maxLength={2}

              />

              <p className="text-red">{error.makeCode}</p>

            </div>

          </div>

          <div className="row row-gap justify-content-center mt-5 mb-4">

            <div className="col-6 col-lg-2 col-md-3">

              <button type="reset" className="btn btn-outline-primary btn-block" onClick={reset}>Reset</button>

            </div>

            <div className="col-6 col-lg-2 col-md-3">

              <button type="submit" className="btn btn-primary btn-block" onClick={handleSubmit} >Submit</button>

            </div>

          </div>

        </div>

      </div>

    </div>

  )
}

export default AddMake