import React, { useState, useEffect } from 'react'
import Alert from '../../../layout/Alert'
import { baseUrl2 } from '../../../../helper/constants/constant'
import axios from 'axios'
import { useHistory } from 'react-router-dom'

const EditCountry = (props) => {
  
  const previousData = props.location.state
  const history = useHistory()
  const [error, seterror] = useState({})
  const [isSubmit, setIsSubmit] = useState(false)
  const [alert, setAlert] = useState(null)
  const [country, setCountry] = useState(previousData.name)
  const onchange = (e) => {
    setCountry(e.target.value)
    seterror({})
    setIsSubmit(false)
  }
  const showAlert = (message, type) => {
    setAlert({
      message: message,
      type: type
    })
    setTimeout(() => {
      setAlert(null)
    }, 2000);
  }

  const reset = () => {
    setCountry('')
  }
  const validation = (country) => {
    const formErrors = {}
    if (!country) {
      formErrors.country = "Country name is required !"
    }
    return formErrors
  }
  const handleSubmit = () => {
    seterror(validation(country))
    setIsSubmit(true)
  }
  useEffect(() => {
    if (Object.keys(error).length == 0 && isSubmit) {
      console.log(country);
      const body = {
        "countryName": country,
        "countryId":previousData.id
      }

      axios.post(`${baseUrl2}/api/v1/location/country/update`, body)
        .then((response) => {
          console.log(response.data);
          if (response.data.status === "success") {
            showAlert("Country edited successfully.", "success")
            setCountry('')
            setTimeout(() => {
              history.push('/Country')

            }, 2000);

          } else {
            showAlert("Some error occur.", "danger")
          }

        })
    }
  }, [error])

  return (
    <div className="dashboard">

    <div className="dashboard-content">

      <div className="col-md-12 form-group d-flex justify-content-between">

        <header className="title-head mb-4 col-md-6">

          <h1 className="h4">Add Country</h1>

        </header>

      </div>

      <Alert alert={alert} />

      <div className="bg-white rounded p-4">

        <div className="row">

          <div className="col-md-6 form-group">

            <label htmlFor="">countryName<span className="text-red">*</span></label>

            <input type="text" className="form-control"
              placeholder="Enter Country name here"
              name='countryName'
              onChange={onchange}
              value={country}

            />

            <p className="text-red">{error.country}</p>

          </div>

        </div>

        <div className="row row-gap justify-content-center mt-5 mb-4">

          <div className="col-6 col-lg-2 col-md-3">

            <button type="reset" className="btn btn-outline-primary btn-block" onClick={reset}>Reset</button>

          </div>

          <div className="col-6 col-lg-2 col-md-3">

            <button type="submit" className="btn btn-primary btn-block" onClick={handleSubmit} >Submit</button>

          </div>

        </div>

      </div>

    </div>

  </div>
  )
}

export default EditCountry