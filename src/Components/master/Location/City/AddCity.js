import React, { useState, useEffect } from 'react'
import Alert from '../../../layout/Alert'
import { baseUrl2 } from '../../../../helper/constants/constant'
import axios from 'axios'
import { useHistory } from 'react-router-dom'

const AddCity = () => {
  const history = useHistory()
  const [error, seterror] = useState({})
  const [isSubmit, setIsSubmit] = useState(false)
  const [alert, setAlert] = useState(null)
  const [data, setData] = useState({
    cityName: '',
    stateId: '',
  })
  const [state, setState] = useState([])

  const getState = async () => {
    await axios.get(`${baseUrl2}/api/v1/location/state`)
      .then((response) => {
        if (response.data.status === "success") {
          setState(response.data.data)
        } else {
          setAlert("Some error occur .", "danger")
        }
      })
  }

  useEffect(() => {
    getState()
  }, [])

  const onchange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    seterror({})
    setIsSubmit(false)
  }
  const showAlert = (message, type) => {
    setAlert({
      message: message,
      type: type
    })
    setTimeout(() => {
      setAlert(null)
    }, 2000);
  }

  const reset = () => {
    setData({
      cityName: '',
      stateId: ''
    }
    )
  }
  const validation = (data) => {
    const formErrors = {}
    if (!data.cityName) {
      formErrors.cityName = "City name is required !"
    }
    if (!data.stateId) {
      formErrors.stateId = "Please select State!"
    }
    return formErrors
  }
  const handleSubmit = () => {
    seterror(validation(data))
    setIsSubmit(true)
  }
  useEffect(() => {
    if (Object.keys(error).length == 0 && isSubmit) {
      console.log(data);
      const body = {
        "cityName": data.cityName,
        "stateId": data.stateId,
      }

      axios.post(`${baseUrl2}/api/v1/location/city/create`, body)
        .then((response) => {
          console.log(response.data);
          if (response.data.status === "success") {
            showAlert("State added successfully.", "success")
            setData({
              cityName: '',
              stateId: ''
            }
            )
            setTimeout(() => {
              history.push('/City')

            }, 2000);

          } else {
            showAlert("Some error occur.", "danger")
          }

        })
    }
  }, [error])

  return (

    <div className="dashboard">

      <div className="dashboard-content">

        <div className="col-md-12 form-group d-flex justify-content-between">

          <header className="title-head mb-4 col-md-6">

            <h1 className="h4">Add City</h1>

          </header>

        </div>

        <Alert alert={alert} />

        <div className="bg-white rounded p-4">

          <div className="row">

            <div className="col-md-6 form-group">

              <label htmlFor="">State<span className="text-red">*</span></label>

              <select name="stateId" id="" className="form-control"
                onChange={onchange}
                value={data.stateId}
                data-size="8" title="Select State ">
                <option value="">Select State</option>
                {
                  state.map((data) => {
                    return <option key={data.id} value={data.id}>{data.name}</option>
                  })
                }

              </select>

              <p className="text-red">{error.stateId}</p>

            </div>

            <div className="col-md-6 form-group">

              <label htmlFor="">City Name<span className="text-red">*</span></label>

              <input type="text" className="form-control"
                placeholder="Enter City name here"
                name='cityName'
                onChange={onchange}
                value={data.cityName}

              />

              <p className="text-red">{error.cityName}</p>

            </div>

          </div>

          <div className="row row-gap justify-content-center mt-5 mb-4">

            <div className="col-6 col-lg-2 col-md-3">

              <button type="reset" className="btn btn-outline-primary btn-block" onClick={reset}>Reset</button>

            </div>

            <div className="col-6 col-lg-2 col-md-3">

              <button type="submit" className="btn btn-primary btn-block" onClick={handleSubmit} >Submit</button>

            </div>

          </div>

        </div>

      </div>

    </div>
  )
}

export default AddCity