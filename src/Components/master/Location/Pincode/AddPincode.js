import React, { useState, useEffect } from 'react'
import Alert from '../../../layout/Alert'
import { baseUrl2 } from '../../../../helper/constants/constant'
import { avoidAlphabets } from '../../../../helper/validation/keypressValidation'
import axios from 'axios'
import { useHistory } from 'react-router-dom'

const AddPincode = () => {
  const history = useHistory()
  const [error, seterror] = useState({})
  const [isSubmit, setIsSubmit] = useState(false)
  const [alert, setAlert] = useState(null)
  const [data, setData] = useState({
    pinCode: '',
    cityId: '',
  })
  const [city, setCity] = useState([])

  const getCity = async () => {
    await axios.get(`${baseUrl2}/api/v1/location/city`)
      .then((response) => {
        if (response.data.status === "success") {
          setCity(response.data.data)
        } else {
          setAlert("Some error occur .", "danger")
        }
      })
  }

  useEffect(() => {
    getCity()
  }, [])

  const onchange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    seterror({})
    setIsSubmit(false)
  }
  const showAlert = (message, type) => {
    setAlert({
      message: message,
      type: type
    })
    setTimeout(() => {
      setAlert(null)
    }, 2000);
  }

  const reset = () => {
    setData({
      pinCode: '',
      cityId: '',
    }
    )
  }
  const validation = (data) => {
    const formErrors = {}
    if (!data.pinCode) {
      formErrors.pinCode = "pincode is required !"
    }
    if (!data.cityId) {
      formErrors.cityId = "Please select City!"
    }
    return formErrors
  }
  const handleSubmit = () => {
    seterror(validation(data))
    setIsSubmit(true)
  }
  useEffect(() => {
    if (Object.keys(error).length == 0 && isSubmit) {
      console.log(data);
      const body = {
        "pincodeName": data.pinCode,
        "cityId": data.cityId,
      }

      axios.post(`${baseUrl2}/api/v1/location/pincode/create`, body)
        .then((response) => {
          console.log(response.data);
          if (response.data.status === "success") {
            showAlert("Pincode added successfully.", "success")
            setData({
              pinCode: '',
              cityId: ''
            }
            )
            setTimeout(() => {
              history.push('/Pincode')

            }, 2000);

          } else {
            showAlert("Some error occur.", "danger")
          }

        })
    }
  }, [error])


  const [fileError, setFileError] = useState()
  const [file, setFile] = useState()
  const onFileChange = (e) => {
    setFileError('')
    setFile(e.target.files[0])

  }


  const uploadFile = () => {
    setFileError('Please Upload a CSV File here.')
    if (file.type === "text/csv") {
      setFileError('')
      let formData = new FormData()
      formData.append('uploadfile', file)
      axios.post(`${baseUrl2}/api/v1/location/pincode/uploadfile`, formData)
        .then((response) => {
          if (response.data.status === "success") {
            showAlert("Pincode Added successfully.", "success")
            setTimeout(() => {
              history.push('/Pincode')
            }, 2000);

          } else {
            showAlert("Some error occur.", "danger")
          }
        }).catch((error)=>{
          showAlert("Some error occur.", "danger")
        })

    }
    

  }


  return (

    <div className="dashboard">

      <div className="dashboard-content">

        <div className="col-md-12 form-group d-flex justify-content-between">

          <header className="title-head mb-4 col-md-6">

            <h1 className="h4">Add Pincode</h1>

          </header>

          <div className="form-group col-md-4 ">

            <label for="exampleFormControlFile1">Upload a CSV file here.</label>

            <input type="file" className="form-control-file" id="exampleFormControlFile1"
              onChange={onFileChange}

            />
            <p className='text-red'>{fileError}</p>

          </div>

          <div className="col-6 col-lg-2 col-md-3">

            <button type="submit" className="btn btn-primary btn-block" onClick={uploadFile}>Upload</button>

          </div>

        </div>

        <Alert alert={alert} />

        <div className="bg-white rounded p-4">

          <div className="row">

            <div className="col-md-6 form-group">

              <label htmlFor="">City<span className="text-red">*</span></label>

              <select name="cityId" id="" className="form-control"
                onChange={onchange}
                value={data.cityId}
                data-size="8" title="Select State ">
                <option value="">Select City</option>
                {
                  city.map((data) => {
                    return <option key={data.id} value={data.id}>{data.name}</option>
                  })
                }

              </select>

              <p className="text-red">{error.cityId}</p>

            </div>

            <div className="col-md-6 form-group">

              <label htmlFor="">Pincode<span className="text-red">*</span></label>

              <input type="text" className="form-control"
                placeholder="Enter pincode here"
                name='pinCode'
                onChange={onchange}
                value={data.pinCode}
                onKeyPress={avoidAlphabets}
                maxLength={6}
              />

              <p className="text-red">{error.pinCode}</p>

            </div>

          </div>

          <div className="row row-gap justify-content-center mt-5 mb-4">

            <div className="col-6 col-lg-2 col-md-3">

              <button type="reset" className="btn btn-outline-primary btn-block" onClick={reset}>Reset</button>

            </div>

            <div className="col-6 col-lg-2 col-md-3">

              <button type="submit" className="btn btn-primary btn-block" onClick={handleSubmit} >Submit</button>

            </div>

          </div>

        </div>

      </div>

    </div>
  )
}

export default AddPincode