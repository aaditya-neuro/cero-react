import React, { useState, useEffect } from 'react'
import Alert from '../../../layout/Alert'
import { baseUrl2 } from '../../../../helper/constants/constant'
import axios from 'axios'
import { useHistory } from 'react-router-dom'

const AddState = () => {
  const history = useHistory()
  const [error, seterror] = useState({})
  const [isSubmit, setIsSubmit] = useState(false)
  const [alert, setAlert] = useState(null)
  const [data, setData] = useState({
    countryId:'',
    stateName:''
  })
  const [country, setCountry] = useState([])

  const getCountry = async () => {
    await axios.get(`${baseUrl2}/api/v1/location/country`)
      .then((response) => {
        if (response.data.status === "success") {
          setCountry(response.data.data)
        } else {
          setAlert("Some error occur .", "danger")
        }
      })
  }
 
  useEffect(() => {
    getCountry()
  }, [])

  const onchange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    seterror({})
    setIsSubmit(false)
  }
  const showAlert = (message, type) => {
    setAlert({
      message: message,
      type: type
    })
    setTimeout(() => {
      setAlert(null)
    }, 2000);
  }

  const reset = () => {
    setData({
      countryId:'',
      stateName:''
    }
    )
  }
  const validation=(data)=>{
    const formErrors = {}
    if (!data.stateName) {
      formErrors.stateName = "state name is required !"
    }
    if (!data.countryId) {
      formErrors.countryId = "Please select country!"
    }
    return formErrors
  }
  const handleSubmit = () => {
    seterror(validation(data))
    setIsSubmit(true)
  }
  useEffect(() => {
    if (Object.keys(error).length == 0 && isSubmit) {
      console.log(data);
      const body = {
       "stateName":data.stateName,
       "countryId":data.countryId
      }

      axios.post(`${baseUrl2}/api/v1/location/state/create`, body)
        .then((response) => {
          console.log(response.data);
          if (response.data.status === "success") {
            showAlert("State added successfully.", "success")
            setData({
              countryId:'',
              stateName:''
            }
            )
            setTimeout(() => {
              history.push('/State')

            }, 2000);

          } else {
            showAlert("Some error occur.", "danger")
          }

        })
    }
  }, [error])

  return (
    <div className="dashboard">

      <div className="dashboard-content">

        <div className="col-md-12 form-group d-flex justify-content-between">

          <header className="title-head mb-4 col-md-6">

            <h1 className="h4">Add State</h1>

          </header>

        </div>

        <Alert alert={alert} />

        <div className="bg-white rounded p-4">

          <div className="row">

            <div className="col-md-6 form-group">

              <label htmlFor="">Country<span className="text-red">*</span></label>

              <select name="countryId" id="" className="form-control"
                onChange={onchange}
                value={data.countryId}
                data-size="8" title="Select Country ">
                <option value="">Select Country</option>
                {
                  country.map((data) => {
                    return <option key={data.id} value={data.id}>{data.name}</option>
                  })
                }

              </select>

              <p className="text-red">{error.countryId}</p>

            </div>

            <div className="col-md-6 form-group">

              <label htmlFor="">State Name<span className="text-red">*</span></label>

              <input type="text" className="form-control"
                placeholder="Enter State name here"
                name='stateName'
                onChange={onchange}
                value={data.stateName}

              />

              <p className="text-red">{error.stateName}</p>

            </div>

          </div>

          <div className="row row-gap justify-content-center mt-5 mb-4">

            <div className="col-6 col-lg-2 col-md-3">

              <button type="reset" className="btn btn-outline-primary btn-block" onClick={reset}>Reset</button>

            </div>

            <div className="col-6 col-lg-2 col-md-3">

              <button type="submit" className="btn btn-primary btn-block" onClick={handleSubmit} >Submit</button>

            </div>

          </div>

        </div>

      </div>

    </div>
  )
}

export default AddState