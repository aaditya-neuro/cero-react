import React, { useState, useEffect } from 'react'
import Alert from '../../layout/Alert'
import validation from '../../../helper/validation/add_vehicle_scrapPrice_validation'
import { avoidAlphabets } from '../../../helper/validation/keypressValidation'

import { baseUrl2 } from '../../../helper/constants/constant'
import axios from 'axios'
import { useHistory } from 'react-router-dom'

const AddVehicleSrapPrice = () => {
  const history = useHistory()
  const [error, seterror] = useState({})
  const [isSubmit, setIsSubmit] = useState(false)
  const [alert, setAlert] = useState(null)
  const [data, setData] = useState({
    vehicleCategoryId:"",
    vehicleTypeId: "",
    vehicleScrapBasePrice:"",
    status: ""
  })
  const [vehicleType, setVehicleType] = useState([])
  const [vehicleCategory, setVehicleCategory] = useState([])

  const getVehicleType = async () => {
    await axios.get(`${baseUrl2}/api/v1/vehicleType`)
      .then((response) => {
        if (response.data.status === "success") {
          setVehicleType(response.data.data)
        } else {
          setAlert("Some error occur .", "danger")
        }
      })
  }
  const getVehicleCategory = async () => {
    await axios.get(`${baseUrl2}/api/v1/vehicleCategory`)
      .then((response) => {
        if (response.data.status === "success") {
          setVehicleCategory(response.data.data)
        } else {
          setAlert("Some error occur .", "danger")
        }
      })
  }
  useEffect(() => {
    getVehicleType()
    getVehicleCategory()
  }, [])

  const onchange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    seterror({})
    setIsSubmit(false)
  }
  const onStatusChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    seterror({})
    setIsSubmit(false)
  }
  const showAlert = (message, type) => {
    setAlert({
      message: message,
      type: type
    })
    setTimeout(() => {
      setAlert(null)
    }, 2000);
  }

  const reset = () => {
    setData({
      vehicleCategory: "",
      status: ""
    })
  }
  const handleSubmit = () => {
    seterror(validation(data))
    setIsSubmit(true)
  }
  useEffect(() => {
    if (Object.keys(error).length == 0 && isSubmit) {
      console.log(data);
      const body = {
       "vehicleCategoryId":JSON.parse(data.vehicleCategoryId),
       "vehicleTypeId":JSON.parse(data.vehicleTypeId),
       "vehicleScrapBasePrice":JSON.parse(data.vehicleScrapBasePrice),
        "status": JSON.parse(data.status)
      }

      axios.post(`${baseUrl2}/api/v1/vehicleScrapPrice/create`, body)
        .then((response) => {
          console.log(response.data);
          if (response.data.status === "success") {
            showAlert("Vehicle Scrap Price added successfully.", "success")
            setData({
              vehicleCategory: "",
              status: ""
            })
            setTimeout(() => {
              history.push('/vehicleScrapPrice')

            }, 2000);

          } else {
            showAlert("Some error occur.", "danger")
          }

        })
    }
  }, [error])

  return (

    <div className="dashboard">

      <div className="dashboard-content">

        <div className="col-md-12 form-group d-flex justify-content-between">

          <header className="title-head mb-4 col-md-6">

            <h1 className="h4">Add Vehicle Scrap Price</h1>

          </header>

        </div>

        <Alert alert={alert} />

        <div className="bg-white rounded p-4">

          <div className="row">

            <div className="col-md-6 form-group">

              <label htmlFor="">vehicle Category<span className="text-red">*</span></label>

              <select name="vehicleCategoryId" id="" className="form-control"
                onChange={onchange}
                value={data.vehicleCategoryId}
                data-size="8" title="Select vehicle Category">
                <option value="">Select vehicle Category</option>
               
                {
                  vehicleCategory.map((data) => {
                    return <option key={data.id} value={data.id}>{data.vehicleCategoryName}</option>
                  })
                }

              </select>

              <p className="text-red">{error.vehicleCategoryId}</p>

            </div>

            <div className="col-md-6 form-group">

              <label htmlFor="">vehicle Type<span className="text-red">*</span></label>

              <select name="vehicleTypeId" id="" className="form-control"
                onChange={onchange}
                value={data.vehicleTypeId}
                data-size="8" title="Select vehicle Type ">
                <option value="">Select vehicle Type</option>
                {
                  vehicleType.map((data) => {
                    return <option key={data.id} value={data.id}>{data.vehicleTypeName}</option>
                  })
                }

              </select>

              <p className="text-red">{error.vehicleTypeId}</p>

            </div>

            <div className="col-md-6 form-group">

              <label htmlFor="">Vehicle Scrap Base Price<span className="text-red">*</span></label>

              <input type="text" className="form-control"
                placeholder="Enter vehicle Scrap base price here"
                name='vehicleScrapBasePrice'
                onKeyPress={avoidAlphabets}
                onChange={onchange}
                value={data.vehicleScrapBasePrice}
                maxLength={5}

              />

              <p className="text-red">{error.vehicleScrapBasePrice}</p>

            </div>

            <div className="col-md-6 form-group">

              <label htmlFor="">Status<span className="text-red">*</span></label>

              <div className="form-check d-flex">

                <input className="form-check-input" type="radio"
                  name="status"
                  id="exampleRadios1"
                  onChange={onStatusChange}
                  value={1}
                />

                <label className="form-check-label" for="exampleRadios1">
                  Active
                </label>

                <div className="form-check mx-2">

                  <input className="form-check-input" type="radio"
                    name="status"
                    id="exampleRadios1"
                    onChange={onStatusChange}
                    value={0} />

                  <label className="form-check-label" for="exampleRadios2">
                    Inactive
                  </label>

                </div>

              </div>

            <p className="text-red">{error.status}</p>

            </div>

          </div>

          <div className="row row-gap justify-content-center mt-5 mb-4">

            <div className="col-6 col-lg-2 col-md-3">

              <button type="reset" className="btn btn-outline-primary btn-block" onClick={reset}>Reset</button>

            </div>

            <div className="col-6 col-lg-2 col-md-3">

              <button type="submit" className="btn btn-primary btn-block" onClick={handleSubmit} >Submit</button>

            </div>

          </div>

        </div>

      </div>

    </div>
  )
}

export default AddVehicleSrapPrice