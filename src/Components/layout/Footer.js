import React from 'react'
//import {  } from 'react-router-dom';
import {Container} from 'react-bootstrap';
import accelo from '../../assets/img/accelo-logo.png'
import mmtc from '../../assets/img/mmtc-logo.png'

export default class Footer extends React.Component {
	
	render(){    
		return(
              <>
                {/* Footer Start */}
                <footer className='siteFooter'>
                <div className='footerLogo mb-3'>
                    <Container>
                        <div className='d-flex justify-content-center ventureLogo'>
                            <div className='shadow bg-white rounded mt-3 py-3 d-flex align-items-center justify-content-between'>
                                <img src={accelo}className='mx-5' alt=''></img>
                                <img src={mmtc} className='mx-5' alt=''></img>
                            </div>                             
                        </div>
                        <div className='text-center py-2'>A Mahindra Accelo &amp; MSTC Venture</div>
                    </Container>
                </div>
                <div className='footerSocial py-3 text-center'>
                    <Container>
                        <a href='https://www.twitter.com' rel='noreferrer' title='Twitter' target={'_blank'}><i className='fa fa-twitter'></i></a>
                        <a href='https://www.facebook.com' rel='noreferrer' title='facebook' target={'_blank'}><i className='fa fa-facebook'></i></a>
                        <a href='https://www.instagram.com' rel='noreferrer' title='Instagram' target={'_blank'}><i className='fa fa-instagram'></i></a>
                        <a href='https://www.youtube.com' rel='noreferrer' title='YouTube' target={'_blank'}><i className='fa fa-youtube-play'></i></a>
                        <a href='https://www.linkedin.com' rel='noreferrer' title='Linkedin' target={'_blank'}><i className='fa fa-linkedin'></i></a>
                    </Container>
                </div>
                <div className='footerWhatsapp py-3 text-center'>
                    <Container>
                        <a href='https://api.whatsapp.com/send?phone=+918851793319' rel='noreferrer' title='Twitter' target={'_blank'}><i className='fa fa-whatsapp'></i> Connect on WhatsApp</a>                            
                    </Container>
                </div>
                <div className='footerCopyRight py-3 text-center'>
                    <Container>
                        &copy; 2022 Mahindra MSTC Recycling Pvt. Ltd. All Rights Reserved                           
                    </Container>
                </div>
                </footer>
                {/* Footer End */}
              </>
		)
	}
}




