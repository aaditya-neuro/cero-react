import React, { useEffect } from 'react'
import { Link, useHistory } from 'react-router-dom'

import dashbord from '../../assets/images/dashboard-nav.svg'
import user from '../../assets/images/group-nav.svg'
import branch from '../../assets/images/branch-nav.svg'
import $ from 'jquery'


const Sidebar = () => {
    useEffect(() => {
        $('.nav-collapse-btn, .nav-blocker').click(function () {
            $('body').toggleClass('is-open');

        });

    }, [])

    const history = useHistory()

    return (

        <nav className="vertical-menu bg-dark col-4" >

            <div className="nav-blocker">&nbsp;</div>

            <button className="nav-collapse-btn"><span></span></button>

            <div className="menu-logo">

                <Link to="/" className="navbar-brand-logo">

                    <img src="img/logo-cero-white.png" alt="Mahindra" />

                </Link>

            </div>

            <div className="navbar-menu">
                <ul className="nav flex-column" id="CollapseNav">
                    <li className="nav-item"><Link to="./Lead" className="nav-link">
                        <span className="nav-icon"><img src={dashbord} alt="Dashboard Icon" /></span>
                        <span className="nav-title">Dashboard</span></Link>
                    </li>
                    <li className={`nav-item ${history.location.pathname == "/addLead" || "/Lead" ? "active" : ""}`}><Link to="#" className={`nav-link ${history.location.pathname == "/Lead" || "/addLead" ? "collapsed" : ""}`} data-toggle="collapse" data-target="#leadManagement">
                        <span className="nav-icon"><img src={branch} alt="Lead" /></span>
                        <span className="nav-title">Lead Management</span></Link>
                        <ul className={`sub-menu collapse ${history.location.pathname == "/Lead" || "/add-lead" ? "" : "show"}`} id="leadManagement" data-parent="#CollapseNav">
                            <li className="nav-item"><Link to="./Lead" className="nav-link">Leads</Link></li>
                            {/* <li className="nav-item"><Link to="./add-lead" className="nav-link">Add Lead</Link></li> */}
                            <li className="nav-item"><Link to="./Lead-customerCare" className="nav-link">Leads customer care</Link></li>
                            {/* <li className="nav-item"><Link to="./edit-Lead-customerCare" className="nav-link">Edit Lead customerCare</Link></li> */}
                        </ul>
                    </li>
                    <li className={`nav-item ${history.location.pathname == "/User" || "roles" ? "active" : ""}`}><Link to="#" className={`nav-link ${history.location.pathname == "/User" || "roles" ? "collapsed" : ""}`} data-toggle="collapse" data-target="#userManagement">
                        <span className="nav-icon"><img src={user} alt="User" /></span>
                        <span className="nav-title">User Management</span></Link>
                        <ul className={`sub-menu collapse ${history.location.pathname == "/User" || "roles" ? "" : "show"}`} id="userManagement" data-parent="#CollapseNav">
                            <li className="nav-item"><Link className="nav-link" to="./User">User</Link></li>
                            <li className="nav-item"><Link className="nav-link" to="./roles">Roles</Link></li>
                        </ul>
                    </li>
                    <li className={`nav-item ${history.location.pathname == "/User" || "roles" ? "active" : ""}`}><Link to="#" className={`nav-link ${history.location.pathname == "/User" || "roles" ? "collapsed" : ""}`} data-toggle="collapse" data-target="#userMaster">

                        <span className="nav-icon"><img src={user} alt="User" /></span>

                        <span className="nav-title">Master</span></Link>

                        <ul className={`sub-menu collapse ${history.location.pathname == "/User" || "roles" ? "" : "show"}`} id="userMaster" data-parent="#CollapseNav">
                            <li className="nav-item"><Link className="nav-link" to="./plant"> Plant</Link></li>
                            <li className="nav-item"><Link className="nav-link" to="./vehicleCategory">Vehicle category</Link></li>
                            <li className="nav-item"><Link className="nav-link" to="./vehicleType">vehicle Type</Link></li>
                            <li className="nav-item"><Link className="nav-link" to="./vehicleScrapPrice">Vehicle Scrap Price</Link></li>
                            <li className="nav-item"><Link className="nav-link" to="./Make">Make</Link></li>
                            <li className="nav-item"><Link className="nav-link" to="./Model">Model</Link></li>
                            
                        </ul>
                    </li>
                    <li className="nav-item">
                                <Link className={`nav-link collapsed`} data-toggle="collapse" data-target="#locationLink">
                                    <span className="nav-icon"></span>
                                    <span className="nav-title">Location</span></Link>
                                 <ul className={`sub-menu collapse ${history.location.pathname == "/User" || "roles" ? "" : "show"}`} id="locationLink" data-parent="#CollapseNav">

                                    {/* <li className="nav-item"><Link className="nav-link" to="./Country"> Country</Link></li> */}
                                    <li className="nav-item"><Link className="nav-link" to="./State">State</Link></li>
                                    <li className="nav-item"><Link className="nav-link" to="./City">City</Link></li>
                                    {/* <li className="nav-item"><Link className="nav-link" to="./Pincode">Pincode</Link></li> */}
                                </ul>
                            </li>
                        </ul>
            </div>
        </nav>
    )
}

export default Sidebar