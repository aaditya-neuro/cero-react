import React, { useContext } from 'react'
import bell from '../../assets/images/bell.svg'
import avatar from '../../assets/images/avtar.jpg'
import { Link, useHistory, Redirect } from 'react-router-dom'
import $ from 'jquery'
import AuthContext from '../../context/Auth/authContext'

const Header = () => {
  
  const userName=localStorage.getItem("userName")
  const context = useContext(AuthContext)
  const { isAuthenticated,setIsauthenticated } = context

  const history = useHistory()
  const logout=()=>{
    setIsauthenticated(false)
  localStorage.clear("auth-token")

  history.push("/")
  }

  const goBack = () => {
    if (history.location.pathname === "/Lead") {
      
      <Redirect to="/Lead" />

    } else {
      history.goBack()
    }

  }
  return (

    <header className="page-topbar">

      <div className="d-flex h-100">

        <div className=" col-sm-6 p-0 mt-3" id='goback'>

          <button type="button" className="btn text-primary"  onClick={goBack} > &laquo; Back </button>

        </div>

        <div className="col-sm-6 p-0 d-flex justify-content-end">

          <div className="header-notify dropdown">

            <a href="javascript:void(0);" className="nav-link" data-toggle="dropdown">

              <img src={bell} alt="Notification icon" />

              <span className="badge">3</span>

            </a>

            <div className="dropdown-menu dropdown-menu-right dropdown-lg">

              <h6 className="dropdown-item-text font-15 m-0 py-3 border-bottom d-flex justify-content-between align-items-center">

                Notifications <span className="badge badge-primary badge-pill">2</span>

              </h6>

              <div className="notification-menu">

                <a href="#" className="dropdown-item py-3">

                  <small className="float-right text-muted pl-2">2 min ago</small>

                  <div className="media">

                    <div className="media-body align-self-center ml-2 text-truncate">

                      <h6 className="my-0 font-weight-normal text-dark">Your order is placed</h6>

                      <small className="text-muted mb-0">Dummy text of the printing and industry.</small>

                    </div>

                  </div>

                </a>

              </div>

            </div>

          </div>

          <div className="header-avtar dropdown">

            <a href="javascript:void(0);" className="nav-link" data-toggle="dropdown">

              <img src={avatar} alt="Avtar" className="rounded avtar-img" />

              <span className="avtar-title pl-2">{userName}</span>

            </a>

            <div className="dropdown-menu dropdown-menu-right">

              <Link to="/profile" className="dropdown-item">Profile</Link>

              <div className="dropdown-divider mb-0"></div>

              <Link onClick={logout} className="dropdown-item">Logout</Link>

            </div>

          </div>

        </div>

      </div>

    </header>
  )
}

export default Header