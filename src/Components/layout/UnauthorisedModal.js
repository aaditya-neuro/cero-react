import React from 'react'
import { useHistory } from 'react-router-dom'

const UnauthorisedModal = () => {
const history = useHistory()
const back = () => {
    history.goBack()
  }

  return (

    <div class="modal fade " id="unauthorisedUser" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true" >

    <div class="modal-dialog modal-dialog-centered">

      <div class="modal-content">

        <div class="modal-header">

          <h4 class="modal-title" id="staticBackdropLabel">Unauthorised User Please go back to previous page</h4>

        </div>

        <div class="modal-body d-flex justify-content-center">

          <button class="btn btn-primary" type="submit" onClick={back}>Ok</button>

        </div>

      </div>

    </div>

  </div>

  )
}

export default UnauthorisedModal