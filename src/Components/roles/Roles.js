import React, { useState, useEffect } from 'react'
import { Link, useHistory } from 'react-router-dom';

import addUser from '../../assets/images/add-user.svg';
import deleteIcon from '../../assets/images/delete.svg';
import editIcon from '../../assets/images/edit.svg';
import viewIcon from '../../assets/images/view-icon.svg';
import currentDate from '../../helper/dates/currentDate'
import baseUrl from '../../helper/constants/constant';
import axios from 'axios';
import $ from 'jquery'
import Alert from '../layout/Alert';

const Roles = () => {
  const history = useHistory()
  var currentMonth = currentDate().currentMonth
  var todayDate = currentDate().currentDate
  var currentYear = currentDate().currentYear

  var date = `${currentMonth} ${todayDate}, ${currentYear}`

  const authToken = localStorage.getItem('auth-token')

  const [roles, setRoles] = useState([])

  const getRolesList = async () => {

    const headers = {
      Authorization: `Bareer ${authToken}`
    }
    axios.get(`${baseUrl}/api/v1/roles/list`, { headers: headers })
      .then(response => {
        if (response.data.status === "success") {
          setRoles(response.data.data);
        }

      })
      .catch((error) => {
        console.log('error ' + error);
      });
  };
  const [alert, setAlert] = useState(null)
  const showAlert = (message, type) => {
    setAlert({
      message: message,
      type: type
    })
    setTimeout(() => {
      setAlert(null)
    }, 2000);

  }

  useEffect(() => {
    getRolesList()
  }, [])
  const editItem = (item) => {
    history.push({
      pathname: '/edit-roles',
      state: item
    })

  }
  const [deleteId, setdeleteId] = useState()
  const deleteItem = (id) => {
    $('#deleteRoleModal').modal('show')
    setdeleteId(id);
  }

  const deleteRole = () => {
    const headers = {
      Authorization: `Bareer ${authToken}`
    }
    axios.get(`${baseUrl}/api/v1/roles/delete/${deleteId}`, { headers: headers })
      .then((response) => {
        console.log(response.data);
        // problem
        if (response.data.status === "success") {
          setTimeout(() => {
            $('#deleteRoleModal').modal('hide')
            showAlert("Role Deleted successfully.", "success")
          }, 1000);
          // for just mounting the component again 

        } else {
          showAlert("Some error occur.", "danger")
        }
      }
      )

  }


  return (
    <>

      <div className="dashboard">

        <div className="dashboard-content">

          <div className="modal" id='deleteRoleModal' tabIndex="-1" style={{ width: "40%", marginLeft: "30%" }}>

            <div className="modal-dialog-centered">

              <div className="modal-content">

                <div className="modal-header">

                  <h5 className="modal-title">Are you sure to delete this role?</h5>

                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">

                    <span aria-hidden="true">&times;</span>

                  </button>

                </div>

                <div className="modal-footer">

                  <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>

                  <button type="button" onClick={deleteRole} className="btn btn-danger">Delete Role</button>

                </div>

              </div>

            </div>

          </div>

          <header className="title-head d-flex align-items-center mb-5">

            <h1 className="h4">User Role</h1>

            <div className="ml-auto">

              <Link to="./add-user" className="btn btn-light icon-right">Add User <img src={addUser} alt="add-user" /></Link>

              <a href="javascript:void(0);" className="btn btn-light ml-2">{date}  : <span className="text-primary">Today</span>

              </a>

            </div>

          </header>
          <Alert alert={alert}/>

          <div className="bg-white rounded p-4">

            <header className="title-head d-flex align-items-center mb-4">

              <h4 className="mb-0">Total Roles :{roles.length}</h4>

              <Link to="./add-role" className="btn btn-outline-primary ml-auto d-flex align-items-center"><span className="icon-fill mr-1"><svg xmlns="http://www.w3.org/2000/svg" width="15.823" height="15.823" viewBox="0 0 15.823 15.823"><g transform="translate(-3.375 -3.375)"><path d="M18.616,15.345H15.345v3.271h-1.6V15.345H10.477v-1.6h3.271V10.477h1.6v3.271h3.271Z" transform="translate(-3.26 -3.26)" fill="#17469e" /><path d="M11.286,4.972A6.311,6.311,0,1,1,6.821,6.821a6.288,6.288,0,0,1,4.465-1.849m0-1.6A7.911,7.911,0,1,0,19.2,11.286a7.91,7.91,0,0,0-7.911-7.911Z" fill="#17469e" /></g></svg></span>Add New</Link>

            </header>

            <div className="table-responsive">

              <table className="table v-aligm-middle medium m-0">

                <thead>

                  <tr>

                    <th>Role Name</th>

                    <th>Role type</th>

                    <th width="200">Action</th>

                  </tr>

                </thead>

                <tbody className="text-muted">
                  {
                    roles.map((ele) => {
                      return <tr key={ele.id}>
                        <td>{ele.roleName}</td>
                        <td>{ele.role_type_id}</td>
                        <td>
                          <button type="button" className="btn btn-sm btn-light btn-icon pl-2 pr-2" onClick={() => editItem(ele)} title="Edit"><img src={editIcon} alt="Edit Icon" /></button>
                          <button type="button" className="btn btn-sm btn-light btn-icon pl-2 pr-2" onClick={() => deleteItem(ele.id)} title="Delete"><img src={deleteIcon} alt="Trash Icon" /></button>
                        </td>

                      </tr>
                    })
                  }


                </tbody>

              </table>

            </div>

          </div>

          <div className="d-flex mt-5">

            <div className="list-number">Page 1 of 150</div>

            <nav aria-label="Table navigation" className="w-100">

              <ul className="pagination justify-content-center">

                <li className="page-item"><a className="page-link active" href="#">1</a></li>

                <li className="page-item"><a className="page-link" href="#">2</a></li>

                <li className="page-item"><a className="page-link" href="#">3</a></li>

                <li className="page-item"><a className="page-link" href="#">4</a></li>

                <li className="page-item"><a className="page-link" href="#">5</a></li>

                <li className="page-item"><a className="page-link" href="#">6</a></li>

                <li className="page-item">

                  <a className="page-link next" href="#">Next</a>

                </li>

              </ul>

            </nav>

          </div>

        </div>

      </div>


    </>
  )
}

export default Roles