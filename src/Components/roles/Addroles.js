import React, { useState, useEffect } from 'react';
import { Accordion } from 'react-bootstrap';
import Alert from '../layout/Alert'
import axios from 'axios'
import baseUrl from '../../helper/constants/constant'
import $ from 'jquery'
import slugify from 'react-slugify';

var finalobj = {};

const Addroles = () => {

	const [ss, setss] = useState({})
	const [users, setUsers] = useState([]);
	const [selectUserType, setSelectUserType] = useState([])
	const authToken = localStorage.getItem('auth-token')

	const getModuleActions = async () => {

		const headers = {
			Authorization: `Bareer ${authToken}`
		}
		axios.get(`${baseUrl}/api/v1/modules/modules-list-with-mapped-actions`, { headers: headers })
			.then(response => {
				setUsers(response.data.data);

			})
			.catch((error) => {
				console.log('error ' + error);
			});
	};


	useEffect(() => {
		getModuleActions()
		getUserType()
	}, []);
	const [alert, setAlert] = useState(null)


	// for showing alerts 
	const showAlert = (message, type) => {
		setAlert({
			message: message,
			type: type
		})
		setTimeout(() => {
			setAlert(null)
		}, 2000);
	}

	const [input, setInput] = useState({
		roleName: "",
		roleSlug: "",
	})


	const handleOnChange = (e) => {
		setInput({ ...input, [e.target.name]: e.target.value })

	}

	// const onHandleChange = (e) => {
	// 	if(parentname === "selectAll") {
	// 	let tempUser = users.ModulesActions.map(ele => {return {...ele, isChecked : checked} })
	// 	setUsers(tempUser)

	// 	} else {
	// 	let tempUser = users.ModulesActions.map(ele => ele.name === name ? {...ele, isChecked : checked} : ele)
	// 	  setUsers(tempUser)
	// 	}
	// 	}

	$(document).ready(function () {

		$('#users').on('change', function () {

			let vr = $(this).attr('checkclass');
			$("." + vr).prop('checked', this.checked);
		    let childclass= $(this).attr('actionclass');
			//let checkedval=$('input[class="'+childclass+'"]:checked').val();
			//let checkedval=$('.'+childclass).val();
		// 	let checkedval=$('input.'+childclass+':checked').each(function () {
		// 		var status = (this.checked ? $(this).val() : "");
				
		// 		//var id = $(this).attr("id"); 
		//    //	
		//    console.log(status);

	    //     });

		let checkedval=$('input.'+childclass+':checked').val();
		console.log("DDDDD",checkedval)
			


			

		});

		$('.child').on('click', function () {
			console.log("Chile call")
			let vr1 = $(this).attr('parentclass');
			let len = $(this).attr('parentlength');;
			var favorite = [];
			$.each($("." + vr1 + ":checked"), function () {
				favorite.push($(this).val());
			});
			console.log(favorite);
			let ckecklen = favorite.length;
			if (len == ckecklen) {
				$(".pr" + vr1).prop('checked', true);
			}
			else {
				$(".pr" + vr1).prop('checked', false);
			}

		});
	});

	const handleChange = (e) => {
		
		const checked = e.target.checked;
		const attributeval = e.target.getAttribute("parent_id");

		// to get the checked value
		//console.log(len);
		const checkedValue = e.target.value;
		// console.log(checkedValue);

		if (checked) {
			if (finalobj[attributeval] === undefined) {
				finalobj = { ...finalobj, [attributeval]: [checkedValue] };
				//finalarray.push(attributeval);
			}
			else {
				var as = finalobj[attributeval];
				console.log(typeof as);
				//as.push(checkedValue);
				if (as.indexOf(checkedValue) < 0) {
					as.push(checkedValue);
					//finalobj={...finalobj,[attributeval]:[...as]};
				}


			}
		}
		else {
			var as = finalobj[attributeval];
			if (as.indexOf(checkedValue) > 0) {
				as.pop(checkedValue);
				finalobj = { ...finalobj, [attributeval]: [...as] };
			}
		}

		// to get the checked name
		// const checkedName = e.target.name;
		//console.log(checkedValue,checkedName,attributeval);
		//console.log(finalarray);
		//then you can do with the value all you want to do with it.

	}

	const submit = (e) => {
		e.preventDefault()
		const body = {

			"roleName": input.roleName,

			"roleSlug": slugify(input.roleName),

			"role_permissions": JSON.stringify(finalobj),

			"roleTypeId": JSON.parse(userType),

			"parentId": 0,

			"sequence": 1,

			"status": 1

		};
		// //console.log(JSON.stringify(finalobj));
		// //  console.log(input);
		console.log(body)
		// const headers = {
		// 	Authorization: `Bareer ${authToken}`
		//   }
		// axios.post(`${baseUrl}/api/v1/roles/create`, body,{ headers: headers })
		// .then((response) => {
		// 	console.log(response);
		//    if (response.data.status === "success") {
		// 	showAlert("role added successfully.", "success")
		// 	console.log(body)
		// 	 setInput({
		// 		roleName:"",
		// 		roleSlug:"",
		// 	})

		//   }
		//   else {
		//     showAlert("Some error occured.", "danger")
		//   }
		// });

	}
	const [userType, setUserType] = useState()
	const onUserTypeChange = (e) => {
		setUserType(e.target.value)
	}
	console.log(userType);

	const getUserType = async () => {

		const headers = {
			Authorization: `Bareer ${authToken}`
		}
		axios.get(`${baseUrl}/api/v1/roletypes/list`, { headers: headers })
			.then(response => {
				setSelectUserType(response.data.data);

			})
			.catch((error) => {
				console.log('error ' + error);
			});
	};

	return (
		<>
			<form >

				<div className='dashboard'>

					<div className='dashboard-content'>

						<header class="title-head mb-4">

							<h1 class="h4">Add Role</h1>

						</header>



						<div class="bg-white rounded p-4 mb-4">

							<div class="row">

								<div className="col-md-4 form-group">

									<label htmlFor="">User Type<span className="text-red">*</span></label>

									<select name='userType' id="" className="form-control"
										onChange={onUserTypeChange}
										data-size="8" title="Select Role"
										value={userType}
									>
										<option value="">Select User Type</option>
										{
											selectUserType.map((res) => {
												return <option key={res.id} value={res.id}>{res.role_type_name}</option>

											})
										}

									</select>

									{/* <p className="text-red">{error.roleTypeId}</p> */}

								</div>


								<div className="col-md-4 form-group">

									<label htmlFor="">Role Name<span className="text-red">*</span></label>

									<input type="text" className="form-control"
										placeholder="Enter Role name here"
										onChange={handleOnChange}
										value={input.roleName}
										name="roleName"
									/>

								</div>

								<div className="col-md-4 form-group">

									<label htmlFor="">Slug Name<span className="text-red">*</span></label>

									<input type="text" className="form-control"
										placeholder="Enter Slug name here"
										value={slugify(input.roleName)}
										onChange={handleOnChange}
										name="roleSlug"
									/>

								</div>

							</div>

						</div>
						<Alert alert={alert} />

						<div class="bg-white rounded p-4">

							<header class="title-head mb-4">

								<h4 class="mb-0">Permissions</h4>

							</header>

							<header class="title-head d-flex align-items-center mb-4" />

							<div class="btn-checkbox">

								<input type="checkbox" id="SelectAllRole" name="check-all" />
								<label for="SelectAllRole">Select All<span class="text-red">* </span></label>

							</div>

							<br />

							<br />

							{/*  <button class="btn btn-outline-primary ml-auto d-flex align-items-center toggle-all" type="button" id="CollapseAll">Expand/Collapse All</button>*/}

							<Accordion alwaysOpen>
								<Accordion.Item eventKey="1">

									{
										users.map((user) => {
											return (

												<>

													<Accordion.Header><div class="form-check">
														<input className={`parent pr${user.moduleName}`} type="checkbox"
                                                             	id={user.moduleName}
															checkclass={user.moduleName}
															value={user.id}
															actionClass={user.moduleName}
															/>
														<label class="form-check-label" for="defaultCheck1">
															{user.moduleName}
														</label>

													</div>

													</Accordion.Header>

													<Accordion.Body>
														{
															user.ModulesActions.map((ele) => {
																var length = (user.ModulesActions).length

																return <div class="form-check">
																	<input className={`child ${user.moduleName}`} type="checkbox" value={ele.id}
																		parent_id={user.id}
																		parentclass={user.moduleName}
																		parentlength={length}
																		id={ele.id} onChange={handleChange} />
																	<label class="form-check-label" for="defaultCheck1">
																		{ele.actionName}
																	</label>
																</div>

															})
														}
													</Accordion.Body>
												</>
											)
										})
									}
								</Accordion.Item>
							</Accordion>

							<br />

							<div className="row row-gap justify-content-center">

								<div className="col-md-6 col-lg-2 col-md-3">

									<button type="submit" className="btn btn-primary btn-block" onClick={submit} >Submit</button>

								</div>

							</div>

						</div>

					</div>

				</div>

			</form>

		</>
	);
}


export default Addroles