import React, { useState, useEffect, useContext } from 'react'
import baseUrl from '../../helper/constants/constant'
import validate from '../../helper/validation/formValidation'
import AuthContext from '../../context/Auth/authContext'
import { useHistory } from "react-router-dom";
import axios from 'axios'
import $ from 'jquery'
import Alert from '../layout/Alert';


const Login = () => {
    let history = useHistory();
    useEffect(() => {
        $(".toggle-password").click(function () {

            $(this).toggleClass("is-text");

            var input = $($(this).attr("toggle"));

            if (input.attr("type") == "password") {

                input.attr("type", "text");

            } else {

                input.attr("type", "password");

            }

        });
    }, [])

    const [user, setUser] = useState({
        email: "",
        password: ""
    })
    const context = useContext(AuthContext)
    const { isAuthenticated, setIsauthenticated } = context
    const [error, seterror] = useState({})
    const [isSubmit, setisSubmit] = useState(false)
    const [alert, setAlert] = useState(null)
    const [passwordToggle, setPasswordToggle] = useState(false)

    const showAlert = (message, type) => {
        setAlert({
          message: message,
          type: type
        })
        setTimeout(() => {
          setAlert(null)
        }, 2000);
      }
    const onChange = (e) => {
        seterror({})
        setUser({ ...user, [e.target.name]: e.target.value })
    }
    const checkUser = () => {
        seterror(validate(user))
        setisSubmit(true)
    }

    const getUser = async(user) => {
        
        const body = {
            userEmail:user.email,
            password:user.password
        };
       await axios.post(`${baseUrl}/api/v1/users/login`, body)
            .then((response) => {
                console.log(response.data);
                if (response.data.status === 'true') {
                    const authToken = response.data.data.token
                    localStorage.setItem("auth-token", authToken)
                    const userName = response.data.data.userName
                    localStorage.setItem("userName", userName)
                    setIsauthenticated(true)
                    showAlert("Login Successfully","success")
                    setTimeout(() => {
                        history.push("/Lead")
                    }, 2000);
                }
                else {
                    showAlert("Invalid Credentials","danger")
                }
            }).catch((error)=>{
                showAlert("Invalid Credentials","danger")
            });
    }

    useEffect(() => {
        if (Object.keys(error).length === 14 && isSubmit) {
            getUser(user)
        }
    }, [error])

    return (

        <div className="d-flex h-100 login-wrapper">

            <div className="col-sm-6 d-flex h-100  align-items-center p-0 bg-dark text-white text-center">

                <div className="w-100 p-3">

                    <h4 className="mb-4">WELCOME TO</h4>

                    <img src="img/logo-cero-white.png" alt=""></img>

                </div>

            </div>

            <div className="col-sm-6 d-flex align-items-center bg-white">

                <div className="login-form p-0 col-7 m-auto">

                    <h4 className="text-primary text-uppercase mb-3">GET Started</h4>

                  <Alert alert={alert}/>

                    <div className="form-group mb-4 input-user">

                        <input type="text" placeholder="User Name" className="form-control"
                            onChange={onChange}
                            value={user.email}
                            name="email"
                        />

                        <p className="text-red">{error.email}</p>

                    </div>

                    <div className="form-group input-pass mb-4">

                        <input type={`${passwordToggle == false ? "password" : "text"}`} placeholder="Password" id="password-field" className="form-control input-pass"
                            onChange={onChange}
                            value={user.password}
                            name="password"
                        />


                        <span toggle="#password-field" className="toggle-password"></span>

                        <p className="text-red">{error.password}</p>

                    </div>

                    <div className="d-flex align-items-center">

                        <a href="#" className="text-muted medium fgt-pass">Forgot Password?</a>

                        <input type="submit" value="Log in" className="btn btn-primary ml-auto" onClick={checkUser} />

                    </div>

                </div>

            </div>

        </div>
    )
}

export default Login