import React from 'react'

import userIcon from '../../assets/images/user-icon.svg'
import envelope from '../../assets/images/envelope-icon.svg'
import addUser from '../../assets/images/add-user.svg'
import phone from '../../assets/images/phone-icon.svg'

import { Link } from 'react-router-dom'

const Profile = () => {
    return (
        <>

            <div className="dashboard">

                <div className="dashboard-content">

                    <header className="title-head d-flex align-items-center mb-md-5 mb-4">

                        <h1 className="h4 mb-0">Profile</h1>

                        <Link to="./profile-edit" className="btn btn-primary ml-auto">Edit</Link>

                    </header>

                    <div className="bg-white rounded p-md-5 p-4">

                        <div className="row profile-row">

                            <div className="col-sm-6 mb-sm-5 mb-4">

                                <div className="media media-view">

                                    <img src={userIcon} alt="icon" className="media-img mr-3" />

                                    <div className="media-body">

                                        <span className="d-block text-muted medium mb-2">Name</span>

                                        <h5 className="d-block text-black bold">

                                           Pankaj Sharma

                                        </h5>

                                    </div>

                                </div>

                            </div>

                            <div className="col-sm-6 mb-sm-5 mb-4">

                                <div className="media media-view">

                                    <img src={envelope} alt="icon" className="media-img mr-3" />

                                    <div className="media-body">

                                        <span className="d-block text-muted medium mb-2">Email</span>

                                        <h5 className="d-block text-black bold">b1@gmail.com</h5>

                                    </div>

                                </div>

                            </div>

                            <div className="col-sm-6 mb-sm-0 mb-4">

                                <div className="media media-view">

                                    <img src={addUser} alt="icon" className="media-img mr-3" />

                                    <div className="media-body">

                                        <span className="d-block text-muted medium mb-2">Role</span>

                                        <h5 className="d-block text-black bold">Branch Manager</h5>

                                    </div>

                                </div>

                            </div>

                            <div className="col-sm-6">

                                <div className="media media-view">

                                    <img src={phone} alt="icon" className="media-img mr-3" />

                                    <div className="media-body">

                                        <span className="d-block text-muted medium mb-2">Phone</span>

                                        <h5 className="d-block text-black bold">8599588589</h5>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>



        </>
    )
}

export default Profile