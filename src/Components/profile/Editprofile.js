import React, { useState, useEffect } from 'react'
import Alert from '../layout/Alert'

import validate from '../../helper/validation/formValidation'
import { avoidAlphabets, avoidSpace } from '../../helper/validation/keypressValidation'

const Editprofile = () => {

    const [userData, setUserData] = useState({
        email: "",
        contactNumber: "",
        oldPassword: "",
        newPassword: "",
        confirmPassword: ""
    })
    const [error, seterror] = useState({})
    const [isSubmit, setIsSubmit] = useState(false)
    const [alert, setAlert] = useState(null)

    const showAlert = (message, type) => {
        setAlert({
            message: message,
            type: type
        })
        setTimeout(() => {
            setAlert(null)
        }, 2000);
    }

    useEffect(() => {
        if (Object.keys(error).length == 9 && isSubmit) {
            showAlert("Profile updated successfully.", "success")
            console.log(userData);
            setUserData({
                email: "",
                contactNumber: "",
                oldPassword: "",
                newPassword: "",
                confirmPassword: ""
            })
            // api call here


        }

    }, [error])

    const onChange = (e) => {
        setUserData({ ...userData, [e.target.name]: e.target.value })
        seterror({})
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        seterror(validate(userData))
        setIsSubmit(true)
    }

    return (
        <>

            <div className="dashboard">


                <div className="dashboard-content">

                    <header className="title-head mb-md-5 mb-4">

                        <h1 className="h4 mb-0">Edit Profile</h1>

                    </header>

                    <Alert alert={alert} />

                    <div className="bg-white rounded p-4">

                        <form action="./profile_edit_submit" method="post" role="form">

                            <h2 className="mb-4 h4 bolder text-muted">Account Information</h2>

                            <div className="row">

                                <div className="col-md-4 form-group">

                                    <label for="">Email<span className="text-red">*</span></label>

                                    <input type="email" name='email' className="form-control" value={userData.email}
                                        onChange={onChange}
                                        placeholder="Enter email address" />

                                    <p className='text-red'>{error.email}</p>

                                </div>

                                <div className="col-sm-4 form-group">

                                    <label for="">Phone<span className="text-red">*</span></label>

                                    <input type="tel" className="form-control"
                                        placeholder="Enter contact number here"
                                        onKeyPress={avoidAlphabets}
                                        onChange={onChange}
                                        value={userData.contactNumber}
                                        name="contactNumber"
                                        maxLength={10}
                                    />

                                    <p className='text-red'>{error.contactNumber}</p>

                                </div>

                            </div>

                            <hr />

                            <h2 className="mb-4 h4 bolder text-muted">Change Password</h2>

                            <div className="row">

                                <div className="col-md-4 form-group">

                                    <label for="">Old Password<span className="text-red">*</span></label>

                                    <input type="password" className="form-control" name="oldPassword" id="oldPassword"
                                        value={userData.oldPassword}
                                        onChange={onChange}
                                    />

                                    <p className='text-red'>{error.oldPassword}</p>

                                </div>

                                <div className="col-sm-4 form-group">

                                    <label for="">Password<span className="text-red">*</span></label>

                                    <input type="password" className="form-control" name="newPassword" id="newPassword"
                                        value={userData.newPassword}
                                        onChange={onChange}
                                    />

                                    <p className='text-red'>{error.newPassword}</p>

                                </div>

                                <div className="col-sm-4 form-group">

                                    <label for="">Confirm Password<span className="text-red">*</span></label>

                                    <input type="password" className="form-control" name="confirmPassword" id="confirmPassword"
                                        value={userData.confirmPassword}
                                        onChange={onChange}
                                    />

                                    <p className='text-red'>{error.confirmPassword}</p>

                                </div>

                            </div>

                            <div className="row row-gap justify-content-center mt-md-4">

                                <div className="col-sm-6 col-lg-2 col-md-3">

                                    <button type="submit" name='submit' onClick={handleSubmit} className="btn btn-primary btn-block">Update</button>

                                </div>

                            </div>

                        </form>

                    </div>

                </div>

            </div>


        </>
    )
}

export default Editprofile