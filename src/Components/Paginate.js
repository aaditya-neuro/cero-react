import React, { useEffect, useState } from 'react';
import ReactPaginate from 'react-paginate';
import deleteIcon from '../assets/images/delete.svg';
import editIcon from '../assets/images/edit.svg';
import { useHistory } from 'react-router-dom';
import $ from 'jquery'

function Items(props) {
  
    const history = useHistory()
    
    const deleteItem = (id) => {

        props.getIdItems(id);
        $(props.deleteModalId).modal('show')

    }
    const editItem = (item) => {
        history.push({
			pathname: `${props.path}`,
			state: item
		})
    }
    return (
        <>
            {props.currentItems &&

                props.currentItems.map((item) => (

                    <tr key={item.id}>

                        <td>{item.name}</td>

                        <td>{item.email}</td>

                        <td>{item.contactNumber}</td>

                        <td>{item.location}</td>

                        <td>{item.model}</td>

                        <td>{item.make}</td>

                        <td>{item.leadStatus}</td>

                        <td>
                            <button type="button" onClick={() => editItem(item)} className="btn btn-sm btn-light btn-icon pl-2 pr-2" title="Edit"><img src={editIcon} alt="Edit Icon" /></button>
                            <button type="button" onClick={() => deleteItem(item.id)} className="btn btn-sm btn-light btn-icon pl-2 pr-2" title="Delete"><img src={deleteIcon} alt="Trash Icon" /></button>


                        </td>
                    </tr>
                ))}

        </>
    );
}

function PaginatedItems(props) {
    const [itemId, setItemId] = useState(null)
    const getIdItems = (index) => {
        setItemId(index)
    };
    //sending id to the component
    props.getId(itemId)
    const path = props.path
    const deleteModalId= props.deleteModalId

    // We start with an empty list of items.
    const [currentItems, setCurrentItems] = useState(props.items);
    const [pageCount, setPageCount] = useState(0);
    // Here we use item offsets; we could also use page offsets
    // following the API or data you're working with.
    const [itemOffset, setItemOffset] = useState(0);
    const [currentPage, setCurrentPage] = useState(1)

    useEffect(() => {
        // Fetch items from another resources.
        const endOffset = itemOffset + props.itemsPerPage;
        setCurrentItems(props.items.slice(itemOffset, endOffset));
        setPageCount(Math.ceil(props.items.length / props.itemsPerPage));
    }, [itemOffset, props.itemsPerPage]);

    // Invoke when user click to request another page.
    const handlePageClick = (event) => {
        const newOffset = (event.selected * props.itemsPerPage) % props.items.length;
        setItemOffset(newOffset);
        setCurrentPage(event.selected + 1)
    };
    return (
        <>
        {/* <div style={{backgroundColor:"beige"}}> */}

            <Items currentItems={currentItems} getIdItems={getIdItems} path={path} deleteModalId={deleteModalId} />
        {/* </div> */}

            <div className="d-flex mt-5 ">

                <div className="list-number">Page {currentPage} of {pageCount}</div>

                <nav aria-label="Table navigation">

                    <ul className="pagination justify-content-center">

                        <ReactPaginate
                            nextLabel="next>"
                            onPageChange={handlePageClick}
                            pageRangeDisplayed={3}
                            marginPagesDisplayed={2}
                            pageCount={pageCount}
                            previousLabel="<prev"
                            pageClassName="page-item"
                            pageLinkClassName="page-link"
                            previousClassName="page-item"
                            previousLinkClassName="page-link"
                            nextClassName="page-item"
                            nextLinkClassName="page-link"
                            breakLabel="..."
                            breakClassName="page-item"
                            breakLinkClassName="page-link"
                            containerClassName="pagination"
                            activeClassName="active"
                            renderOnZeroPageCount={null}
                        />
                    </ul>

                </nav>

            </div>
        </>
    );
}
export default PaginatedItems

