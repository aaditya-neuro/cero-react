import React, { useState, useEffect } from 'react'
import Select from 'react-select';
import validation from '../../helper/validation/add_plant_validation'
import Alert from '../layout/Alert'
import axios from 'axios';
import { baseUrl2 } from '../../helper/constants/constant';
import { useHistory } from 'react-router-dom';
import $ from 'jquery'
import UnauthorisedModal from '../layout/UnauthorisedModal';

const Editplant = (props) => {
  const history = useHistory()

  const previousData = props.location.state;

  const [error, setError] = useState({})

  const [isSubmit, setIsSubmit] = useState(false)

  const [alert, setAlert] = useState(null)

  const [selectedItems, setSelectedItems] = useState()

  const [data, setData] = useState(
    {
      plantName: "",
      state: "",
      city: "",
      address: "",
    }
  )

  const [state, setState] = useState([])
  const [city, setCity] = useState([])


  const getState = async () => {
    await axios.get(`${baseUrl2}/api/v1/location/state`)
      .then((response) => {
        if (response.data.status === "success") {
          setState(response.data.data)
        } else {
          setAlert("Some error occur .", "danger")
        }
      })
  }

  // get cities in dropdown based on states when passed in on change func below
  const getCity = async (stateId) => {
    const body = {
      stateId: stateId
    }
    await axios.post(`${baseUrl2}/api/v1/location/city/getByState`, body)
      .then((response) => {
        if (response.data.status === "success") {
          setCity(response.data.data)
        } else {
          setAlert("Some error occur .", "danger")
        }
      })
  }

  useEffect(() => {
    if (!previousData) {
      $("#unauthorisedUser").modal('show')
    } else {
      getState()
      setData({
        plantName: previousData.plant_name,
        state: previousData.state,
        city: previousData.city,
        address: previousData.address,
      })

    }



  }, [])
  useEffect(() => {
    getCity(data.state)
  }, [data.state])


  const [selected, setselected] = useState()

  const handleMulti = (selected) => {
    setError({})
    setselected([...selected]);
    var items = (selected.map((ele) => {
      return ele.value
    }))
    setSelectedItems(items)

  }

  const handleSubmit = () => {
    setError(validation(data))
    setIsSubmit(true)
  }

  useEffect(() => {
    if (Object.keys(error).length == 0 && isSubmit) {
      const { plantName, state, city, address } = data
      const body = {
        ...data,
        plantId: props.location.state.id,
        serviceableCity: selectedItems,
        CreatedBy: 1,
        updatedBy: 1,
        status: 1
      };
      axios.post(`${baseUrl2}/api/v1/plant/update`, body)
        .then((response) => {
          if (response.data.status === "success") {
            showAlert("plant edited successfully.", "success")
            setData({
              plantName: "",
              state: "",
              city: "",
              address: "",
            })
            setselected([])
            setTimeout(() => {
              history.push('/plant')
            }, 2000);
          } else {
            showAlert("Some error occur.", "danger")
          }

        })
    }

  }, [error])

  const onchange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setError({})
    setIsSubmit(false)
    // getCity(data.state)
  }

  const reset = () => {
    setData({
      plantName: "",
      state: "",
      city: "",
      address: "",
    })
    setselected([])
  }

  const showAlert = (message, type) => {
    setAlert({
      message: message,
      type: type
    })
    setTimeout(() => {
      setAlert(null)
    }, 2000);
  }

  return (

    <div className="dashboard">

      <div className="dashboard-content">

        <UnauthorisedModal />

        <div className="col-md-12 form-group d-flex justify-content-between">

          <header className="title-head mb-4 col-md-6">

            <h1 className="h4">Edit Plant</h1>

          </header>

        </div>

        <Alert alert={alert} />

        <div className="bg-white rounded p-4">

          <div className="row">

            <div className="col-md-6 form-group">

              <label htmlFor="">Plant Name<span className="text-red">*</span></label>

              <input type="text" className="form-control"
                placeholder="Enter plant name here"
                name='plantName'
                onChange={onchange}
                value={data.plantName}

              />

              <p className="text-red">{error.plantName}</p>

            </div>

            <div className="col-md-6 form-group">

              <label htmlFor="">State<span className="text-red">*</span></label>

              <select name="state" id="" className="form-control"
                onChange={onchange}
                value={data.state}
                data-size="8" title="Select City"
              >

                <option value="">Select State</option>

                {
                  state.map((data) => {
                    return <option key={data.id} value={data.id}>{data.name}</option>
                  })
                }

              </select>


              <p className="text-red">{error.state}</p>

            </div>

            <div className="col-md-6 form-group">

              <label htmlFor="">City<span className="text-red">*</span></label>

              <select name="city" id="" className="form-control"
                onChange={onchange}
                value={data.city}
                data-size="8" title="Select City">

                <option value="">Select City</option>

                {
                  city.map((data) => {
                    return <option value={data.id}>{data.name}</option>
                  })
                }

              </select>


              <p className="text-red">{error.city}</p>

            </div>

            <div className="col-md-6 form-group">

              <label htmlFor="">Address<span className="text-red">*</span></label>

              <input type="text" className="form-control"
                placeholder="Enter Address here"
                name="address"
                onChange={onchange}
                value={data.address}

              />

              <p className="text-red">{error.address}</p>

            </div>

            <div className="col-md-6 form-group">

              <label htmlFor="">Serviceable City<span className="text-red">*</span></label>

              <Select options={city.map((data) => {
                return { value: data.id, label: data.name }
              })} isMulti
                value={selected}
                onChange={handleMulti}
              />

              <p className="text-red">{error.serviceableCity}</p>

            </div>

          </div>

          <div className="row row-gap justify-content-center mt-5 mb-4">

            <div className="col-6 col-lg-2 col-md-3">

              <button type="reset" className="btn btn-outline-primary btn-block" onClick={reset}>Reset</button>

            </div>

            <div className="col-6 col-lg-2 col-md-3">

              <button type="submit" className="btn btn-primary btn-block" onClick={handleSubmit}>Submit</button>

            </div>

          </div>

        </div>

      </div>

    </div>
  )
}

export default Editplant