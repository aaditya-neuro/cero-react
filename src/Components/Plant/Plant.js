import React, { useState, useEffect } from 'react'
import { Link, useHistory } from 'react-router-dom'

import ReactPaginate from 'react-paginate';
import currentDate from '../../helper/dates/currentDate'
import deleteIcon from '../../assets/images/delete.svg';
import editIcon from '../../assets/images/edit.svg';
import { baseUrl2 } from '../../helper/constants/constant';
import axios from 'axios';
import $ from 'jquery'
import Alert from '../layout/Alert';

const Plant = () => {

  var currentMonth = currentDate().currentMonth
  var todayDate = currentDate().currentDate
  var currentYear = currentDate().currentYear

  var date = `${currentMonth} ${todayDate}, ${currentYear}`

  const [filterData, setFilterData] = useState({
    name: "",
    city: "",
    state: ""
  })

  const onchange = (e) => {
    setFilterData({ ...filterData, [e.target.name]: e.target.value })
    setError('')
  }

  const [error, setError] = useState(null)
  const search = () => {
    if (filterData.name || filterData.state || filterData.city) {
      console.log(filterData);
      setFilterData({
        name: "",
        city: "",
        state: ""
      })

    } else {
      setError("please fill one of these fields.")
    }

  }

  const reset = () => {
    setFilterData({
      name: "",
      city: "",
      state: ""
    })
    setError('')
  }


  const history = useHistory()

  const editPlant = (ele) => {

    history.push({
      pathname: '/edit-plant',
      state: ele
    })
  }

  const [deletePlantId, setDeletePlantId] = useState()
  const Delete = (id) => {
    $('#plantDelete').modal('show')
    setDeletePlantId(id)

  }
  const [deleteUpdate, setdeleteUpdate] = useState()
  const deletePlant = () => {
    console.log(deletePlantId);
    //api call here to delete user
    const body = {
      plantId: deletePlantId
    };

    axios.post(`${baseUrl2}/api/v1/plant/delete`, body)
      .then((response) => {
        console.log(response.data);
        if (response.data.status === "success") {
          setTimeout(() => {
            $('#plantDelete').modal('hide')
            showAlert("Plant Deleted successfully.", "success")
          }, 1000);
          // for just mounting the component again 
          setdeleteUpdate(new Date)

        } else {
          showAlert("Some error occur.", "danger")
        }
      }
      )
  }
  // for alerts 
  const [alert, setAlert] = useState(null)

  const showAlert = (message, type) => {
    setAlert({
      message: message,
      type: type
    })
    setTimeout(() => {
      setAlert(null)
    }, 2000);

  }
  const [items, setItems] = useState([])
  const getPlant = async () => {
    const res = await fetch(
      `${baseUrl2}/api/v1/plant`
    );
    const data = await res.json();
    if (data.status === "success") {
      setItems(data.data)
    } else {
      setAlert("Some error occur.", "danger")
    }
    return data;
  };

  useEffect(() => {
    getPlant()
    setError('')
  }, [deleteUpdate])


  // pagination
  //   const [items, setItems] = useState([])
  //   const [pageCount, setPageCount] = useState(0)
  //   const [currentPageShow, setCurrentPageShow] = useState(1)
  //   let limit = 5

  //   // run first time to get data from api 
  //   useEffect(() => {
  //     const getComments = async () => {
  //       const res = await fetch(
  //         `http://localhost:3004/comments?_page=1&_limit=${limit}`
  //       )
  //       const data = await res.json();
  //       const total = res.headers.get('x-total-count')
  //       setPageCount(Math.ceil(total/limit))
  //       setItems(data);
  //     }
  //     getComments()
  //   }, [])

  // // run when page changes 

  //   const fetchComments = async (currentPage) => {
  //     const res = await fetch(
  //       `http://localhost:3004/comments?_page=${currentPage}&_limit=${limit}`
  //     );
  //     const data = await res.json();
  //     return data;
  //   };
  // // function to trigger page change in pagination
  //   const handlePageChange = async(data) => {
  //      let currentPage= data.selected +1
  //      setCurrentPageShow(currentPage)
  //    const commentsFromServer = await fetchComments(currentPage)
  //    setItems(commentsFromServer)

  //   }

  return (


    <div className="dashboard">

      <div className="dashboard-content">

        <div className="modal" id='plantDelete' tabIndex="-1" style={{ width: "40%", marginLeft: "30%" }}>

          <div className="modal-dialog-centered">

            <div className="modal-content">

              <div className="modal-header">

                <h5 className="modal-title">Are you sure to delete Plant?</h5>

                <button type="button" className="close" data-dismiss="modal" aria-label="Close">

                  <span aria-hidden="true">&times;</span>

                </button>

              </div>

              <div className="modal-footer">

                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>

                <button type="button" onClick={deletePlant} className="btn btn-danger">Delete Plant</button>

              </div>

            </div>

          </div>

        </div>


        <header className="title-head d-flex align-items-center mb-5">

          <h1 className="h4">Recycling Plants</h1>

          <div className="ml-auto">

            <Link to="/add-plant" className="btn btn-light icon-right">Add Plant <img src="images/add-user.svg" alt="" /></Link>

            <Link to="javascript:void(0);" className="btn btn-light ml-2">{date} : <span className="text-primary">Today</span>

            </Link>

          </div>

        </header>

        <Alert alert={alert} />

        <div className="bg-white rounded p-4 mb-4">

          <header className="title-head  mb-4">

            <h4 className="mb-0">Search Plant</h4>
            <p className='text-red'>{error}</p>

          </header>

          <div className="row">

            <div className="col-md-4 form-group">

              <label for="">Name</label>

              <input type="text" className="form-control" placeholder="Enter name here"
                name='name'
                onChange={onchange}
                value={filterData.name}
              />

            </div>

            <div className="col-md-4 form-group">

              <label for="">City</label>

              <input type="text" className="form-control" placeholder="Enter city here"
                name='city'
                onChange={onchange}
                value={filterData.city}
              />

            </div>

            <div className="col-md-4 form-group">

              <label for="">State</label>

              <input type="text" className="form-control" placeholder="Enter State here"
                name='state'
                onChange={onchange}
                value={filterData.state}
              />

            </div>

          </div>

          <div className="row row-gap justify-content-center">

            <div className="col-6 col-lg-2 col-md-3">

              <button type="reset" className="btn btn-outline-primary btn-block" onClick={reset}>Reset</button>

            </div>

            <div className="col-6 col-lg-2 col-md-3">

              <button type="submit" className="btn btn-primary btn-block" onClick={search}>Search</button>

            </div>

          </div>

        </div>

        <div className="bg-white rounded p-4">

          <header className="title-head mb-4">

            <h4 className="mb-0">Plant's Data</h4>

          </header>

          <div className="table-responsive">

            <table className="table v-aligm-middle medium">

              <thead>

                <tr>

                  <th>Plant Name</th>

                  <th>State</th>

                  <th>City</th>

                  <th>Address</th>

                  <th>Serviceable City</th>

                  <th width="140">Action</th>

                </tr>

              </thead>

              <tbody className="text-muted">
                {
                  items.map((ele) => {
                    return (
                      <tr key={ele.id}>
                        <td>{ele.plant_name}</td>
                        <td>{ele.state}</td>
                        <td>{ele.city}</td>
                        <td>{ele.address}</td>
                        <td>{ele.serviceable_city}</td>
                        <td>
                          <button type="button" onClick={() => editPlant(ele)} className="btn btn-sm btn-light btn-icon pl-2 pr-2" title="Edit"><img src={editIcon} alt="Edit Icon" /></button>
                          <button type="button" onClick={() => Delete(ele.id)} className="btn btn-sm btn-light btn-icon pl-2 pr-2" title="Delete"><img src={deleteIcon} alt="Trash Icon" /></button>
                        </td>
                      </tr>
                    )
                  })
                }
              </tbody>

            </table>

          </div>

        </div>

        {/* <div className="d-flex mt-5">

          <div className="list-number">Page {currentPageShow}  of {pageCount}</div>

          <nav aria-label="Table navigation" className="w-100">

            <ul className="pagination justify-content-center">

              <ReactPaginate
                previousLabel={'previous'}
                nextLabel={"next"}
                breakLabel={'....'}
                pageCount={pageCount}
                marginPagesDisplayed={2}
                pageRangeDisplayed={2}
                onPageChange={handlePageChange}
                containerClassName={'pagination justify-content-center'}
                pageClassName={'page-item'}
                pageLinkClassName={'page-link'}
                previousClassName={'page-item'}
                nextClassName={'page-item'}
                previousLinkClassName={'page-link'}
                nextLinkClassName={'page-link'}
                breakLinkClassName={'page-link'}
                activeClassName={'active'}

              />

            </ul>

          </nav>

        </div> */}

      </div>

    </div>
  )
}

export default Plant