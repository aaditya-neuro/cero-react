import React, { useState, useEffect } from 'react'
import { Link, useHistory } from 'react-router-dom';
import currentDate from '../../helper/dates/currentDate'
import deleteIcon from '../../assets/images/delete.svg';
import editIcon from '../../assets/images/edit.svg';
import addUser from '../../assets/images/add-user.svg';
import Alert from '../layout/Alert'
import baseUrl from '../../helper/constants/constant';
import axios from 'axios';
import $ from 'jquery'


const Users = () => {

  const history = useHistory()
  const authToken = localStorage.getItem('auth-token')

  var currentMonth = currentDate().currentMonth
  var todayDate = currentDate().currentDate
  var currentYear = currentDate().currentYear
  var date = `${currentMonth} ${todayDate}, ${currentYear}`

  const [deleteId, setDeleteId] = useState(null)
  const [alert, setAlert] = useState(null)
  const [deleteUpdate, setDeleteUpdate] = useState(null)

  const [sortedArray, setSortedArray] = useState([])

  const [search, setSearch] = useState({
    name: "",
    email: "",
    contactNumber: ""
  })
  const [items, setItems] = useState([])

  //for getting users list

  const getusers = async () => {
    const headers = {
      Authorization: `Bareer ${authToken}`
    }
    axios.get(`${baseUrl}/api/v1/users/list`, { headers: headers })
      .then(response => {
        setItems(response.data.data);
      })
      .catch((error) => {
        console.log('error ' + error);
      });
  };


  const [checkAddUser, setCheckAddUser] = useState(false)
  const [checkEditUser, setCheckEditUser] = useState(false)
  const [checkDeleteUser, setCheckDeleteUser] = useState(false)
  const [checkUserList, setCheckUserList] = useState(false)

  //for checking if the loged in user has a permission to see user list.

  const checkForList = () => {
    const headers = {
      Authorization: `Bareer ${authToken}`
    }
    const body = {
      "moduleSlug": "users",
      "actionSlug": "list"
    }
    axios.post(`${baseUrl}/api/v1/users/check-permission`, body, { headers: headers })
      .then(response => {
        if (response.data.data.status === true) {
          setCheckUserList(true)
        }
      })
      .catch((error) => {
        console.log('error ' + error);
      });
  }



  //for checking if the loged in user has a permission to add user so we can hide the add button

  const checkForAdd = () => {
    const headers = {
      Authorization: `Bareer ${authToken}`
    }
    const body = {
      "moduleSlug": "users",
      "actionSlug": "create"
    }
    axios.post(`${baseUrl}/api/v1/users/check-permission`, body, { headers: headers })
      .then(response => {
        if (response.data.data.status === true) {
          setCheckAddUser(true)
        }
      })
      .catch((error) => {
        console.log('error ' + error);
      });
  }
  //for checking if the loged in user has a permission to edit user so we can hide the edit button
  const checkForEdit = () => {
    const headers = {
      Authorization: `Bareer ${authToken}`
    }
    const body = {
      "moduleSlug": "users",
      "actionSlug": "edit"
    }
    axios.post(`${baseUrl}/api/v1/users/check-permission`, body, { headers: headers })
      .then(response => {
        if (response.data.data.status === true) {
          setCheckEditUser(true)
        }
      })
      .catch((error) => {
        console.log('error ' + error);
      });
  }
  //for checking if the loged in user has a permission to delete user so we can hide the delete button
  const checkForDelete = () => {
    const headers = {
      Authorization: `Bareer ${authToken}`
    }
    const body = {
      "moduleSlug": "users",
      "actionSlug": "delete"
    }
    axios.post(`${baseUrl}/api/v1/users/check-permission`, body, { headers: headers })
      .then(response => {
        if (response.data.data.status === true) {
          setCheckDeleteUser(true)
        }
      })
      .catch((error) => {
        console.log('error ' + error);
      });
  }


  useEffect(() => {
    checkForList()
    getusers()
    checkForAdd()
    checkForEdit()
    checkForDelete()
  }, [deleteUpdate])

  const deleteItem = (id) => {
    setDeleteId(id)
    $('#deleteUserModal').modal('show')

  }
  const editItem = (item) => {
    history.push({
      pathname: '/edit-user',
      state: item
    })

  }


  const searchData = () => {

    var newArray = items.filter((user) => {
      if (search.name == user.firstname) {
        return user
      } if (search.contactNumber == user.contactnumber) {
        return user;
      } if (search.email == user.email) {
        return user
      }
    });
    if (newArray.length == 0) {
      showAlert("No data found.", "danger")
      console.log("no data found");

    } else {
      setSortedArray(newArray);
      console.log(sortedArray);
    }
  }

  const reset = () => {
    setSortedArray([])
    setSearch({
      name: "",
      email: "",
      contactNumber: ""
    })

  }

  const deleteUser = () => {
    const headers = {
      Authorization: `Bareer ${authToken}`
    }
    axios.get(`${baseUrl}/api/v1/users/delete/${deleteId}`, { headers: headers })
      .then((response) => {
        console.log(response.data);
        // problem
        if (response.data.status === "success") {
          setTimeout(() => {
            $('#deleteUserModal').modal('hide')
            showAlert("user Deleted successfully.", "success")
          }, 1000);
          // for just mounting the component again 
          setDeleteUpdate(new Date)

        } else {
          showAlert("Some error occur.", "danger")
        }
      }
      )

  }

  const showAlert = (message, type) => {
    setAlert({
      message: message,
      type: type
    })
    setTimeout(() => {
      setAlert(null)
    }, 2000);

  }

  const rule = (id) => {
    history.push({
      pathname: './rules',
      state: id
    })
  }

  return (

    <>

      <div className="dashboard">

        <div className="dashboard-content">

          {/* modal */}

          <div className="modal" id='deleteUserModal' tabIndex="-1" style={{ width: "40%", marginLeft: "30%" }}>

            <div className="modal-dialog-centered">

              <div className="modal-content">

                <div className="modal-header">

                  <h5 className="modal-title">Are you sure to delete this user?</h5>

                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">

                    <span aria-hidden="true">&times;</span>

                  </button>

                </div>

                <div className="modal-footer">

                  <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>

                  <button type="button" onClick={deleteUser} className="btn btn-danger">Delete User</button>

                </div>

              </div>

            </div>

          </div>

          <header className="title-head d-flex align-items-center mb-5">

            <h1 className="h4">User</h1>

            <div className="ml-auto">
              {
                checkAddUser === true ?
                  <Link to="/add-user" className="btn btn-light icon-right">Add User <img src={addUser} alt="add-user" /></Link>
                  : null
              }

              <a href="javascript:void(0);" className="btn btn-light ml-2">{date}  : <span className="text-primary">Today</span>

              </a>

            </div>

          </header>

          <div className="bg-white rounded p-4 mb-4">

            <header className="title-head  mb-4">

              <h4 className="mb-0">Search User</h4>

            </header>

            <div className="row">

              <div className="col-md-4 form-group">

                <label htmlFor="">Name</label>

                <input type="text" className="form-control" placeholder="Enter name here"
                  value={search.name}
                  onChange={(e) => setSearch({ name: (e.target.value) })}
                />

              </div>

              <div className="col-md-4 form-group">

                <label htmlFor="">Email</label>

                <input type="text" className="form-control" placeholder="Enter email here"
                  value={search.email}
                  onChange={(e) => setSearch({ email: (e.target.value) })}
                />

              </div>

              <div className="col-md-4 form-group">

                <label htmlFor="">Contact number</label>

                <input type="text" className="form-control" placeholder="Enter contact number here"
                  value={search.contactNumber}
                  onChange={(e) => setSearch({ contactNumber: (e.target.value) })}
                />

              </div>

            </div>

            <div className="row row-gap justify-content-center">

              <div className="col-6 col-lg-2 col-md-3">

                <button type="reset" onClick={reset} className="btn btn-outline-primary btn-block">Reset</button>

              </div>

              <div className="col-6 col-lg-2 col-md-3">

                <button type="submit" onClick={searchData} className="btn btn-primary btn-block">Search</button>

              </div>

            </div>

          </div>

          <Alert alert={alert} />

          <div className="bg-white rounded p-4">

            <header className="title-head mb-4">

              <h4 className="mb-0">Users</h4>

            </header>

            <div className="table-responsive">

              <table className="table v-aligm-middle medium">

                <thead>
                  {
                    checkUserList === true ?
                      <tr>
                        <th>Name</th>

                        <th>Email</th>

                        <th>Phone No.</th>

                        <th>Address</th>

                        <th>City</th>

                        <th>User Type Id</th>

                        <th>Role Id</th>

                        <th>Status</th>
                        {
                          checkEditUser === true || checkDeleteUser === true ?
                            <th width="200">Action</th> :
                            null
                        }

                      </tr> : null

                  }

                </thead>
                {
                  checkUserList === true ?


                    <tbody className="text-muted">
                      {
                        items.map((item) => (

                          <tr key={item.id}>

                            <td>{item.userName}</td>

                            <td>{item.userEmail}</td>

                            <td>{item.userMobile}</td>

                            <td>{item.userAddress}</td>

                            <td>{item.cityId}</td>

                            <td>{item.roleTypeId}</td>

                            <td>{item.roleId}</td>

                            <td>{item.status}</td>

                            <td>
                              {
                                 checkDeleteUser === true && item.roleId == 2 ?
                                  <button type="button" onClick={() => rule(item.id)} className="btn btn-sm btn-light btn-icon pl-2 pr-2" title="Go to rules">rule</button>
                                  : null
                              }
                              {
                                checkEditUser === true ?
                                  <button type="button" onClick={() => editItem(item)} className="btn btn-sm btn-light btn-icon pl-2 pr-2" title="Edit"><img src={editIcon} alt="Edit Icon" /></button>
                                  : null
                              }
                              {
                                checkDeleteUser === true ?
                                  <button type="button" onClick={() => deleteItem(item.id)} className="btn btn-sm btn-light btn-icon pl-2 pr-2" title="Delete"><img src={deleteIcon} alt="Trash Icon" /></button>
                                  : null
                              }
                            </td>
                          </tr>

                        ))}

                    </tbody> :
                    null
                }

              </table>

            </div>

          </div>

        </div>

      </div>

    </>
  )
}

export default Users