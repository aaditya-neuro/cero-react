import React, { useState, useEffect } from 'react'
import Alert from '../layout/Alert'
import axios from 'axios'
import { avoidAlphabets, avoidSpace } from '../../helper/validation/keypressValidation'
import validate from '../../helper/validation/add_user_validation'
import baseUrl, { baseUrl2 } from '../../helper/constants/constant'
import { useHistory } from 'react-router-dom'
import $ from 'jquery'

const Adduser = () => {
  // for geting roles in dropdown 
  const history = useHistory()
  const back = () => {
    history.goBack()
  }
  const [userType, setUserType] = useState()
  const [selectUserType, setSelectUserType] = useState([])
  const authToken = localStorage.getItem('auth-token')

  const [authForRoute, setAuthForRoute] = useState(false)

  const checkForAdd = () => {
    const headers = {
      Authorization: `Bareer ${authToken}`
    }
    const body = {
      "moduleSlug": "users",
      "actionSlug": "create"
    }
    axios.post(`${baseUrl}/api/v1/users/check-permission`, body, { headers: headers })
      .then(response => {
        console.log(response);
        if (response.data.data.status === true) {
          setAuthForRoute(true)
        } else {
          $("#auth").modal("show")
        }
      })
      .catch((error) => {
        console.log('error ' + error);
      });
  }

  const getUserType = async () => {

    const headers = {
      Authorization: `Bareer ${authToken}`
    }
    axios.get(`${baseUrl}/api/v1/roletypes/list`, { headers: headers })
      .then(response => {
        setSelectUserType(response.data.data);

      })
      .catch((error) => {
        console.log('error ' + error);
      });
  };

  const [selectRoleType, setSelectRoleType] = useState([])

  const getRolesType = async () => {
    const headers = {
      Authorization: `Bareer ${authToken}`
    }
    const body = {
      "roleTypeId": userType
    }
    axios.post(`${baseUrl}/api/v1/roletypes/mapped-role-list-by-roletype`, body, { headers: headers })
      .then(response => {
        setSelectRoleType(response.data.data[0].Roles)
      })
      .catch((error) => {
        console.log('error ' + error);
      });
  };

  const [getCities, setGetCities] = useState([])

  const getCity = async () => {

    axios.get(`${baseUrl2}/api/v1/location/city`)
      .then(response => {
        setGetCities(response.data.data);
      })
      .catch((error) => {
        console.log('error ' + error);
      });
  };
  useEffect(() => {
    checkForAdd()
    getUserType()
    getCity()
  }, [])
  useEffect(() => {
    if (userType) {
      getRolesType()
    }
  }, [userType])


  const [role, setRole] = useState('')
  const [userData, setUserData] = useState({
    userName: "",
    email: "",
    contactNumber: "",
    password: "",
    confirmPassword: "",
    address: "",
    roleId: "",
    roleTypeId: "",
    cityId: '',
    status: ""
  })

  const [error, seterror] = useState({})
  const [isSubmit, setIsSubmit] = useState(false)
  const [alert, setAlert] = useState(null)

  // for showing alerts 
  const showAlert = (message, type) => {
    setAlert({
      message: message,
      type: type
    })
    setTimeout(() => {
      setAlert(null)
    }, 2000);
  }

  useEffect(() => {
    if (Object.keys(error).length === 0 && isSubmit) {
      console.log(userData);
      const authToken = localStorage.getItem("auth-token")
      const body = {
        userName: userData.userName, userEmail: userData.email,
        userMobile: userData.contactNumber,
        password: userData.password,
        confirmPassword: userData.confirmPassword,
        userAddress: userData.address,
        roleId: userData.roleId,
        roleTypeId: userData.roleTypeId,
        cityId: userData.cityId,
        status: userData.status
      };
      const headers = {
        Authorization: `Bareer ${authToken}`
      }

      axios.post(`${baseUrl}/api/v1/users/create`, body, { headers: headers })
        .then((response) => {
          if (response.data.success === true) {
            showAlert("user added successfully.", "success")
            setTimeout(() => {
              history.push('./User')
            }, 2000);
            setUserData({
              userName: "",
              email: "",
              contactNumber: "",
              password: "",
              confirmPassword: "",
              address: "",
              roleId: "",
              roleTypeId: "",
              cityId: '',
              status: ""
            })
            setIsSubmit(false)

          }
          else {
            showAlert("Some error occured.", "danger")
          }
        });

    }

  }, [error])

  const onChange = (e) => {
    setUserData({ ...userData, [e.target.name]: e.target.value })
    seterror({})
  }
  const onRoleChange = (e) => {
    setRole(e.target.value)
    seterror({})
  }
  const onUserTypeChange = (e) => {
    setUserType(e.target.value)
  }

  const onStatusChange = (e) => {
    setUserData({ ...userData, [e.target.name]: e.target.value })
  }
  const reset = () => {
    setUserData({
      name: "",
      userName: "",
      email: "",
      contactNumber: "",
      role: "",
      lastName: "",
      address: "",
      password: "",
      status: "",
    })
    seterror({})
    setRole('')
  }
  // form submission
  const handleSubmit = () => {
    userData.roleTypeId = userType
    userData.roleId = role
    seterror(validate(userData))
    setIsSubmit(true)
  }

  return (
    <>
      {
        authForRoute === true ?


          <div className="dashboard">

            <div className="dashboard-content">

              <header className="title-head mb-4">

                <h1 className="h4">Add User</h1>

              </header>

              <Alert alert={alert} />

              <div className="bg-white rounded p-4">

                <div className="row">

                  <div className="col-md-6 form-group">

                    <label htmlFor="">User Type<span className="text-red">*</span></label>

                    <select name='userType' id="" className="form-control"
                      onChange={onUserTypeChange}
                      data-size="8" title="Select Role"
                      value={userType}
                    >
                      <option value="">Select User Type</option>
                      {
                        selectUserType.map((res) => {
                          return <option key={res.id} value={res.id}>{res.role_type_name}</option>

                        })
                      }

                    </select>

                    <p className="text-red">{error.roleTypeId}</p>

                  </div>


                  <div className="col-md-6 form-group">

                    <label htmlFor="">Role<span className="text-red">*</span></label>

                    <select name='role' id="" className="form-control"
                      onChange={onRoleChange}
                      data-size="8" title="Select Role"
                      value={role}
                    >

                      <option value="">Select Role</option>
                      {
                        selectRoleType.map((res) => {
                          return <option key={res.id} value={res.role_type_id}>{res.roleName}</option>

                        })
                      }

                    </select>

                    <p className="text-red">{error.roleId}</p>

                  </div>

                  <div className="col-md-6 form-group">

                    <label htmlFor="">User Name<span className="text-red">*</span></label>

                    <input type="text" className="form-control"
                      placeholder="Enter user name here"
                      onChange={onChange}
                      value={userData.userName}
                      name="userName"

                    />

                    <p className="text-red">{error.userName}</p>

                  </div>

                  <div className="col-md-6 form-group">

                    <label htmlFor="">Email<span className="text-red">*</span></label>

                    <input type="email" className="form-control"
                      placeholder="Enter email here"
                      onKeyPress={avoidSpace}
                      onChange={onChange}
                      value={userData.email}
                      name="email"
                    />

                    <p className="text-red">{error.email}</p>

                  </div>

                </div>

                <div className="row">

                  <div className="col-md-6 form-group">

                    <label htmlFor="">Contact Number<span className="text-red">*</span></label>

                    <input type="tel" className="form-control"
                      placeholder="Enter contact number here"
                      onKeyPress={avoidAlphabets}
                      onChange={onChange}
                      value={userData.contactNumber}
                      name="contactNumber"
                      maxLength={10}
                    />

                    <p className="text-red">{error.contactNumber}</p>

                  </div>

                  <div className="col-md-6 form-group">

                    <label htmlFor="">Address<span className="text-red">*</span></label>

                    <input type="text" className="form-control"
                      placeholder="Enter Address here"
                      onChange={onChange}
                      value={userData.address}
                      name="address"

                    />

                    <p className="text-red">{error.address}</p>

                  </div>

                  <div className=" col-md-6 form-group">

                    <label for="exampleInputPassword1">Password</label>

                    <input type="password" className="form-control" id="exampleInputPassword1"
                      placeholder="Enter password here"
                      onKeyPress={avoidSpace}
                      onChange={onChange}
                      value={userData.password}
                      name="password"
                    />

                    <p className="text-red">{error.password}</p>

                  </div>

                  <div className=" col-md-6 form-group">

                    <label for="exampleInputPassword1">Confirm Password</label>

                    <input type="text" className="form-control" id="exampleInputPassword1"
                      placeholder="Enter password here"
                      onKeyPress={avoidSpace}
                      onChange={onChange}
                      value={userData.confirmPassword}
                      name="confirmPassword"
                    />

                    <p className="text-red">{error.confirmPassword}</p>

                  </div>

                </div>

                <div className="row">

                  <div className="col-md-6 form-group">

                    <label htmlFor="">City<span className="text-red">*</span></label>

                    <select name="cityId" id="" className="form-control"
                      onChange={onChange}
                      value={userData.cityId}
                      data-size="8" title="Select City">

                      <option value="">Select City</option>
                      {
                        getCities.map((ele) => {
                          return <option key={ele.id} value={ele.id} >{ele.name}</option>
                        })
                      }

                    </select>

                    <p className="text-red">{error.cityId}</p>

                  </div>

                  <div className="form-check">
                    <label htmlFor="">Status<span className="text-red">*</span></label>
                    <div className="row col-md-6 d-flex">
                      <input className="form-check-input" type="radio"
                        name="status"
                        id="exampleRadios1"
                        onChange={onStatusChange}
                        value="1"
                      />
                      <label className="form-check-label" for="exampleRadios1">
                        Active
                      </label>
                    </div>

                    <div className="form-check">
                      <input className="form-check-input" type="radio"
                        name="status"
                        id="exampleRadios2"
                        onChange={onStatusChange}
                        value="0" />

                      <label className="form-check-label" for="exampleRadios2">
                        Inactive
                      </label>

                    </div>

                    <p className="text-red">{error.status}</p>

                  </div>

                </div>


                <div className="row row-gap justify-content-center mt-5 mb-4">

                  <div className="col-6 col-lg-2 col-md-3">

                    <button type="reset" className="btn btn-outline-primary btn-block" onClick={reset}>Reset</button>

                  </div>

                  <div className="col-6 col-lg-2 col-md-3">

                    <button type="submit" className="btn btn-primary btn-block" onClick={handleSubmit}>Submit</button>

                  </div>

                </div>

              </div>

            </div>

          </div> :

          <div className='dashboard'>

            <div className='dashboard-content'>

              <div class="modal fade " id="auth" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true" >

                <div class="modal-dialog modal-dialog-centered">

                  <div class="modal-content">

                    <div class="modal-header">

                      <h4 class="modal-title" id="staticBackdropLabel">Unauthorised User Please go back to previous page</h4>

                    </div>

                    <div class="modal-body d-flex justify-content-center">

                      <button class="btn btn-primary" type="submit" onClick={back}>Ok</button>

                    </div>

                  </div>

                </div>

              </div>

            </div>

          </div>





      }



    </>

  )
}

export default Adduser