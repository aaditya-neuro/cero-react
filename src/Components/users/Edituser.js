import React, { useState, useEffect } from 'react'
import Alert from '../layout/Alert'
import axios from 'axios'
import { avoidAlphabets, avoidSpace } from '../../helper/validation/keypressValidation'
import validate from '../../helper/validation/add_user_validation'
import baseUrl, { baseUrl2 } from '../../helper/constants/constant'
import $ from 'jquery'
import { useHistory } from 'react-router-dom'
import UnauthorisedModal from '../layout/UnauthorisedModal'

const Edituser = (props) => {
  const history = useHistory()
 
  const previousData = props.history.location.state
  console.log(previousData);

  useEffect(() => {
    if (!previousData) {
      $("#unauthorisedUser").modal('show')
    } else {
      getUserType()
      getCity()
      setUserType(previousData.roleTypeId)
      setRole(previousData.roleId)
      setUserData({
        userName: previousData.userName,
        email: previousData.userEmail,
        contactNumber: previousData.userMobile,
        address: previousData.userAddress,
        roleId: '',
        roleTypeId: '',
        cityId: previousData.cityId,
        status: previousData.status

      })
      if (previousData.status === 1) {
        $("#activeStatus").attr("checked", true)
      } else {
        $("#inActiveStatus").attr("checked", true)
      }
    }

  }, [])


  // for geting roles in dropdown 
  const [userType, setUserType] = useState('')
  const [selectUserType, setSelectUserType] = useState([])
  const authToken = localStorage.getItem('auth-token')

  const getUserType = async () => {

    const headers = {
      Authorization: `Bareer ${authToken}`
    }
    axios.get(`${baseUrl}/api/v1/roletypes/list`, { headers: headers })
      .then(response => {
        setSelectUserType(response.data.data);
      })
      .catch((error) => {
        console.log('error ' + error);
      });
  };

  const [selectRoleType, setSelectRoleType] = useState([])

  const getRolesType = async () => {
    const headers = {
      Authorization: `Bareer ${authToken}`
    }
    const body = {
      "roleTypeId": userType
    }
    axios.post(`${baseUrl}/api/v1/roletypes/mapped-role-list-by-roletype`, body, { headers: headers })
      .then(response => {
        setSelectRoleType(response.data.data[0].Roles)
      })
      .catch((error) => {
        console.log('error ' + error);
      });
  };

  const [getCities, setGetCities] = useState([])

  const getCity = async () => {

    axios.get(`${baseUrl2}/api/v1/location/city`)
      .then(response => {
        setGetCities(response.data.data);
      })
      .catch((error) => {
        console.log('error ' + error);
      });
  };

  useEffect(() => {
    if (userType) {
      getRolesType()
    }
  }, [userType])


  const [role, setRole] = useState('')
  const [userData, setUserData] = useState(
    {
      userName: "",
      email: "",
      contactNumber: "",
      address: "",
      roleId: "",
      roleTypeId: "",
      cityId: '',
      status: ""
    }
  )

  const [error, seterror] = useState({})
  const [isSubmit, setIsSubmit] = useState(false)
  const [alert, setAlert] = useState(null)

  // for showing alerts 
  const showAlert = (message, type) => {
    setAlert({
      message: message,
      type: type
    })
    setTimeout(() => {
      setAlert(null)
    }, 2000);
  }

  useEffect(() => {
    if (Object.keys(error).length === 2 && isSubmit) {
      console.log(userData);
      const authToken = localStorage.getItem("auth-token")
      const body = {
        userId: previousData.id,
        userName: userData.userName, userEmail: userData.email,
        userMobile: userData.contactNumber,
        userAddress: userData.address,
        roleId: userData.roleId,
        roleTypeId: userData.roleTypeId,
        cityId: userData.cityId,
        status: userData.status
      };
      const headers = {
        Authorization: `Bareer ${authToken}`
      }

      axios.post(`${baseUrl}/api/v1/users/edit`, body, { headers: headers })
        .then((response) => {
          if (response.data.status === "success") {
            showAlert("user edited successfully.", "success")
            setUserData({
              userName: "",
              email: "",
              contactNumber: "",
              address: "",
              roleId: "",
              roleTypeId: "",
              cityId: '',
              status: ""
            })
            $("#activeStatus").attr("checked", false)
            $("#inActiveStatus").attr("checked", false)
            setTimeout(() => {
              history.push('./User')
            }, 2000);

            setIsSubmit(false)
          }
          else {
            showAlert("Some error occured.", "danger")
          }
        });

    }

  }, [error])

  const onChange = (e) => {
    setUserData({ ...userData, [e.target.name]: e.target.value })
    seterror({})
  }
  const onRoleChange = (e) => {
    setRole(e.target.value)
    seterror({})
  }
  const onUserTypeChange = (e) => {
    setUserType(e.target.value)
  }

  const onStatusChange = (e) => {
    setUserData({ ...userData, [e.target.name]: e.target.value })
  }
  const reset = () => {
    setUserData({
      name: "",
      userName: "",
      email: "",
      contactNumber: "",
      role: "",
      lastName: "",
      address: "",
      password: "",
      status: "",
    })
    seterror({})
    setRole('')
  }
  // form submission
  const handleSubmit = () => {
    userData.roleTypeId = userType
    userData.roleId = role
    seterror(validate(userData))
    setIsSubmit(true)
  }

  return (
    <>

      <div className="dashboard">

        <div className="dashboard-content">

          <UnauthorisedModal/>

          <header className="title-head mb-4">

            <h1 className="h4">Edit User</h1>

          </header>

          <Alert alert={alert} />

          <div className="bg-white rounded p-4">

            <div className="row">

              <div className="col-md-6 form-group">

                <label htmlFor="">User Type<span className="text-red">*</span></label>

                <select name='userType' id="" className="form-control"
                  onChange={onUserTypeChange}
                  data-size="8" title="Select Role"
                  value={userType}
                >
                  <option value="">Select User Type</option>
                  {
                    selectUserType.map((res) => {
                      return <option key={res.id} value={res.id}>{res.role_type_name}</option>

                    })
                  }

                </select>

                <p className="text-red">{error.roleTypeId}</p>

              </div>


              <div className="col-md-6 form-group">

                <label htmlFor="">Role<span className="text-red">*</span></label>

                <select name='role' id="" className="form-control"
                  onChange={onRoleChange}
                  data-size="8" title="Select Role"
                  value={role}
                >

                  <option value="">Select Role</option>
                  {
                    selectRoleType.map((res) => {
                      return <option key={res.id} value={res.role_type_id}>{res.roleName}</option>

                    })
                  }

                </select>

                <p className="text-red">{error.roleId}</p>

              </div>

              <div className="col-md-6 form-group">

                <label htmlFor="">User Name<span className="text-red">*</span></label>

                <input type="text" className="form-control"
                  placeholder="Enter user name here"
                  onChange={onChange}
                  value={userData.userName}
                  name="userName"

                />

                <p className="text-red">{error.userName}</p>

              </div>

              <div className="col-md-6 form-group">

                <label htmlFor="">Email<span className="text-red">*</span></label>

                <input type="email" className="form-control"
                  placeholder="Enter email here"
                  onKeyPress={avoidSpace}
                  onChange={onChange}
                  value={userData.email}
                  name="email"
                />

                <p className="text-red">{error.email}</p>

              </div>

            </div>

            <div className="row">

              <div className="col-md-6 form-group">

                <label htmlFor="">Contact Number<span className="text-red">*</span></label>

                <input type="tel" className="form-control"
                  placeholder="Enter contact number here"
                  onKeyPress={avoidAlphabets}
                  onChange={onChange}
                  value={userData.contactNumber}
                  name="contactNumber"
                  maxLength={10}
                />

                <p className="text-red">{error.contactNumber}</p>

              </div>

              <div className="col-md-6 form-group">

                <label htmlFor="">Address<span className="text-red">*</span></label>

                <input type="text" className="form-control"
                  placeholder="Enter Address here"
                  onChange={onChange}
                  value={userData.address}
                  name="address"

                />

                <p className="text-red">{error.address}</p>

              </div>

            </div>

            <div className="row">

              <div className="col-md-6 form-group">

                <label htmlFor="">City<span className="text-red">*</span></label>

                <select name="cityId" id="" className="form-control"
                  onChange={onChange}
                  value={userData.cityId}
                  data-size="8" title="Select City">

                  <option value="">Select City</option>
                  {
                    getCities.map((ele) => {
                      return <option key={ele.id} value={ele.id} >{ele.name}</option>
                    })
                  }

                </select>

                <p className="text-red">{error.cityId}</p>

              </div>

              <div className="form-check">
                <label htmlFor="">Status<span className="text-red">*</span></label>
                <div className="row col-md-6 d-flex">
                  <input className="form-check-input" type="radio"
                    name="status"
                    id="activeStatus"
                    onChange={onStatusChange}
                    value="1"
                  />
                  <label className="form-check-label" for="exampleRadios1">
                    Active
                  </label>
                </div>

                <div className="form-check">
                  <input className="form-check-input" type="radio"
                    name="status"
                    id="inActiveStatus"
                    onChange={onStatusChange}
                    value="0" />

                  <label className="form-check-label" for="exampleRadios2">
                    Inactive
                  </label>

                </div>

                <p className="text-red">{error.status}</p>

              </div>

            </div>


            <div className="row row-gap justify-content-center mt-5 mb-4">

              <div className="col-6 col-lg-2 col-md-3">

                <button type="reset" className="btn btn-outline-primary btn-block" onClick={reset}>Reset</button>

              </div>

              <div className="col-6 col-lg-2 col-md-3">

                <button type="submit" className="btn btn-primary btn-block" onClick={handleSubmit}>Submit</button>

              </div>

            </div>

          </div>

        </div>

      </div>
    </>
  )
}

export default Edituser