import React, { useState, useEffect } from 'react'
import axios from 'axios';
import { baseUrl2 } from '../../helper/constants/constant';
import Alert from '../layout/Alert';
import deleteIcon from '../../assets/images/delete.svg';
import editIcon from '../../assets/images/edit.svg';
import { useHistory } from 'react-router-dom';
import $ from 'jquery'

const Rules = (props) => {

    const history = useHistory()
    const back = () => {
        history.goBack()
    }

    const userId = props.location.state

    const [inputField, setInputField] = useState([
        { vehicleCategory: '', vehicleType: '', city: '' },
    ])

    const [vehicleCategory, setVehicleCategory] = useState([])
    const getVehicleCategory = async () => {
        const res = await fetch(
            `${baseUrl2}/api/v1/vehicleCategory`
        );
        const data = await res.json();
        if (data.status === "success") {
            setVehicleCategory(data.data)
        } else {
            console.log("error");
        }
        return data;
    };

    const [vehicleType, setVehicleType] = useState([])
    const getVehicleType = async () => {
        const res = await fetch(
            `${baseUrl2}/api/v1/vehicleType`
        );
        const data = await res.json();
        if (data.status === "success") {
            setVehicleType(data.data)
        } else {
            console.log("errpr");
        }
        return data;
    };

    const [cities, setCities] = useState([])
    const getCity = async () => {
        const res = await fetch(
            `${baseUrl2}/api/v1/location/city`
        );
        const data = await res.json();
        if (data.status === "success") {
            setCities(data.data)
        } else {
            console.log("error")
        }
        return data;
    };


    useEffect(() => {
        getVehicleCategory()
        getVehicleType()
        getCity()

        if (!userId) {
            $("#userIdCheck").modal("show")
        }
    }, [])

    const handleChange = (index, e) => {
        const values = [...inputField]
        values[index][e.target.name] = e.target.value
        setInputField(values)
        setIsSubmit(false)
    }

    const addField = () => {
        setInputField([...inputField, { vehicleCategory: '', vehicleType: '', city: '' }])
        seterror([...error, { vehicleCategory: '', vehicleType: '', city: '' }])
        setIsSubmit(false)
    }

    const deleteField = (index) => {
        const values = [...inputField]
        values.splice(index, 1)
        setInputField(values)
        setIsSubmit(false)
    }
    const [alert, setAlert] = useState(null)

    const showAlert = (message, type) => {
        setAlert({
            message: message,
            type: type
        })
        setTimeout(() => {
            setAlert(null)
        }, 2000);

    }
    const [error, seterror] = useState([{}])
    const [isSubmit, setIsSubmit] = useState(false)

    // for showing errors in fields by looping through input fields

    const validate = (values) => {
        let formErrors = [];

        for (let index = 0; index < values.length; index++) {

            let error = {};

            if (!values[index].city) {
                error.city = "city is required"
            }
            if (!values[index].vehicleCategory) {
                error.vehicleCategory = "vehicleCategory is required"
            }
            if (!values[index].vehicleType) {
                error.vehicleType = "vehicleType is required"
            }

            formErrors.push(error);
        }
        return formErrors

    }


    const [item, setitem] = useState([])

    const submit = () => {
        seterror(validate(inputField))
        setIsSubmit(true)
    }
    
    // validating is there any empty field or not for hitng api
    const checkValidation = () => {
       var validateInput = true
        for (let index = 0; index < error.length; index++) {
            if (Object.keys(error[index]).length === 0) {
                validateInput = true
            }
            else {
                validateInput = false
                break;
            }

        }
        return validateInput
    }

    useEffect(async () => {
        if (isSubmit) {
            if (checkValidation()) {
                console.log(inputField);
            }
            setIsSubmit(false)
        }
    }, [error])

    return (

        <div className="dashboard">

            <div className="dashboard-content">

                {/* Modal if someone come from route  */}

                <div class="modal fade " id="userIdCheck" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true" >

                    <div class="modal-dialog modal-dialog-centered">

                        <div class="modal-content">

                            <div class="modal-header">

                                <h4 class="modal-title" id="staticBackdropLabel">Unauthorised User Please go back to previous page</h4>

                            </div>

                            <div class="modal-body d-flex justify-content-center">

                                <button class="btn btn-primary" type="submit" onClick={back}>Ok</button>

                            </div>

                        </div>

                    </div>

                </div>

                <Alert alert={alert} />

                <div className="row">

                    {
                        inputField.map((ele, index) => (
                            <>

                                <div className="col-md-3 form-group">

                                    <label htmlFor="">vehicleCategory</label>

                                    <select type="text" className="form-control" placeholder="Enter vehicle Category here"
                                        value={ele.vehicleCategory}
                                        name='vehicleCategory'
                                        onChange={e => handleChange(index, e)}
                                    >

                                        <option value="">select vehicle Category</option>
                                        {
                                            vehicleCategory.map((ele) => {
                                                return <option key={ele.id} value={ele.id} >{ele.vehicleCategoryName}</option>
                                            })
                                        }

                                    </select>

                                    <p className="text-red">{error[index].vehicleCategory ? error[index].vehicleCategory : ''}</p>

                                </div>

                                <div className="col-md-3 form-group">

                                    <label htmlFor="">vehicle Type</label>

                                    <select type="text" className="form-control" placeholder="Enter vehicle Type  here"
                                        value={ele.vehicleType}
                                        onChange={e => handleChange(index, e)}
                                        name="vehicleType"
                                    >
                                        <option value="">select vehicle Type</option>
                                        {
                                            vehicleType.map((ele) => {
                                                return <option key={ele.id} value={ele.id} >{ele.vehicleTypeName}</option>
                                            })
                                        }

                                    </select>

                                    <p className="text-red">{error[index].vehicleType ? error[index].vehicleType : ''}</p>

                                </div>

                                <div className="col-md-3 form-group">

                                    <label htmlFor="">city</label>

                                    <select type="text" className="form-control" placeholder="Enter city here"
                                        value={ele.city}
                                        onChange={e => handleChange(index, e)}
                                        name="city"
                                    >
                                        <option value="">select city</option>
                                        {
                                            cities.map((ele) => {
                                                return <option key={ele.id} value={ele.id} >{ele.name}</option>
                                            })
                                        }


                                    </select>

                                    <p className="text-red">{error[`${index}`].city ? error[`${index}`].city : ''}</p>

                                </div>

                                <div className="col-md-3 form-group my-4">

                                    <button className='btn btn-primary' onClick={addField}>Add</button>

                                    {
                                        index === 0 ?

                                            null :

                                            <button className='btn btn-danger mx-2' onClick={(index) => deleteField(index)}>Remove</button>

                                    }

                                </div>


                            </>

                        ))
                    }

                </div>

                <div className="row row-gap justify-content-center mt-5 mb-4">

                    <div className="col-12 col-lg-2 col-md-3">

                        <button type="submit" className="btn btn-primary btn-block" onClick={submit}>Submit</button>

                    </div>

                </div>

                <div className="table-responsive">

                    <table className="table v-aligm-middle medium">

                        <thead>

                            <tr>

                                <th>vehicle Category</th>

                                <th>vehicle Type</th>

                                <th>City</th>

                                <th width="140">Action</th>

                            </tr>

                        </thead>

                        <tbody className="text-muted">
                            {
                                item.map((ele) => {
                                    return (
                                        <tr>
                                            <td>{ele.vehicleCategory}</td>
                                            <td>{ele.vehicleType}</td>
                                            <td>{ele.city}</td>

                                            <td>
                                                <button type="button" className="btn btn-sm btn-light btn-icon pl-2 pr-2" title="Edit"><img src={editIcon} alt="Edit Icon" /></button>
                                                <button type="button" className="btn btn-sm btn-light btn-icon pl-2 pr-2" title="Delete"><img src={deleteIcon} alt="Trash Icon" /></button>
                                            </td>
                                        </tr>
                                    )
                                })
                            }
                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    )
}

export default Rules