import React,{useState} from 'react';
import AuthContext from './authContext';
const Authstate = (props) => {
    const [isAuthenticated, setIsauthenticated] = useState(false);
  return (
    < AuthContext.Provider value={{isAuthenticated,setIsauthenticated}}>
    {props.children}
    </AuthContext.Provider>
  )
};

export default Authstate;